# Install

## Requirements

- .NET Framework 4.0 or newer
- Visual Studio 2019
- Build Tools 2017 or up, not exactly required, but you will go through additional step without it.
- SQL Server 2016 or newer. SQL Server 2019 recommended.
- An SQL Server client. Anything will do as long as it can run an SQL Script. SQL Server Manager Studio recommended.

## Installation

1. Clone this repo to where do you want the root of the application will be
   `git clone https://gitlab.com/imama2/panda_mandiri.git`
2. Run the SQL query file located in `./Panda_Mandiri/SQL Queries/script.sql` to generate all the required database and tables for the website to function.
   The script will generate all username located in [NOTES.md](NOTES.md)
3. Open the solution file from the cloned repository, and then build the `Panda_Mandiri_Service` project. Use either `Debug` or `Release` configuration. Remember the configuration.
4. Open the `Panda_Mandiri_Service` project folder and then open `./bin/[Debug|Release]` folder, according to the configuration you've picked. Copy the full path of the `Panda_Mandiri_Service.exe`.
5. If you've downloaded the Build Tools, open the `Development Command Prompt for VS 201X` app in `Run as Administrator` mode. If not, go to the [Additional Steps](#additional-steps) section below.
6. Run the following command, and replace the field as needed:

   ``` dos
    installutil.exe -i [absolute path of the Panda_Mandiri_Service.exe you've copied before]
   ```

7. Open `services.msc`, and make sure the `Panda Mandiri Service` service is there. If it is, you can start the service now. Additionally, you can set the service to run each time your computer starts by setting the startup to Automatic.
8. And that's it! You're ready to run this project either on debug mode or just straight up publish it to IIS. If you want to publish this project, you need to check the additional notes at [NOTES.md](./NOTES.md) first to avoid any errors.

## Additional Steps

If you decide to not to install build tools, you need to do an additional steps to install the service.

1. Locate the `installutil.exe` by opening `C:\Windows\Microsoft.NET\Framework\`.
2. Pick the latest `v4.x` named folder available, and open it. If there's no `v4.x` folder, it means you didn't install .NET Framework 4.x or above. Install it first, and come back here after you've done installing.
3. Copy the folder absolute path, and then open the command line in `Run as Administrator` mode.
4. Open the copied directory in the command line by using the `cd` command:

   ``` dos
    cd [absolute path of the directory]
   ```

5. Go back to step 6 of the [Installation steps](#installation) above.
