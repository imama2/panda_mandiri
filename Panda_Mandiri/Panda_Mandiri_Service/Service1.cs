﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Panda_Mandiri.Models;
using Panda_Mandiri.Models.Extended;
using System.Data.SqlTypes;
using System.Collections;
using System.Timers;
using System.Threading;

namespace Panda_Mandiri_Service
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        SqlConnection connection;
        SqlCommand command;
        System.Timers.Timer timerExecutor = new System.Timers.Timer();

        db_panda_mandiriEntities db = new db_panda_mandiriEntities();

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                connection = new SqlConnection("Data Source=localhost;Initial Catalog=db_panda_mandiri;Persist Security Info=True;User ID=sa;Password=rogan1234*");
                command = new SqlCommand("EXECUTE deleteExpiredVerifyCode; EXECUTE deleteResetPasswordRequest;", connection);
                command.Connection.Open();

                timerExecutor.Elapsed += new ElapsedEventHandler(tmrExecutor_Elapsed);
                timerExecutor.Interval = 1000 * 10;
                timerExecutor.Enabled = true;
                timerExecutor.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void tmrExecutor_Elapsed(object sender, ElapsedEventArgs e)
        {
            command.ExecuteNonQuery();
        }

        protected override void OnStop()
        {
            try
            {
                timerExecutor.Enabled = false;
                timerExecutor.Dispose();
                connection.Dispose();
                command.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
