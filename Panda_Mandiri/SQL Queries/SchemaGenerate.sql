USE [master]
GO
/****** Object:  Database [db_panda_mandiri]    Script Date: 11/2/2020 6:24:22 PM ******/
CREATE DATABASE [db_panda_mandiri]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_panda_mandiri', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_panda_mandiri.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'db_panda_mandiri_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_panda_mandiri_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [db_panda_mandiri] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_panda_mandiri].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_panda_mandiri] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_panda_mandiri] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_panda_mandiri] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_panda_mandiri] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_panda_mandiri] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET RECOVERY FULL 
GO
ALTER DATABASE [db_panda_mandiri] SET  MULTI_USER 
GO
ALTER DATABASE [db_panda_mandiri] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_panda_mandiri] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_panda_mandiri] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_panda_mandiri] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'db_panda_mandiri', N'ON'
GO
ALTER DATABASE [db_panda_mandiri] SET QUERY_STORE = OFF
GO
USE [db_panda_mandiri]
GO
/****** Object:  Table [dbo].[tbl_siswa]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_siswa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama_Lengkap] [varchar](100) NULL,
	[Nama_Panggilan] [varchar](20) NULL,
	[jenis_kelamin] [varchar](10) NULL,
	[id_ortu] [int] NULL,
	[id_kelas] [int] NULL,
	[BirthPlace] [varchar](25) NULL,
	[BirtDate] [datetime] NULL,
	[NIK] [varchar](20) NULL,
	[Anak_Ke] [int] NULL,
	[Jml_Saudara_Kandung] [int] NULL,
	[Golongan_Darah] [varchar](5) NULL,
	[Kelahiran] [varchar](20) NULL,
	[ASI_Berapa_Lama] [int] NULL,
	[Sufor_Berapa_Lama] [int] NULL,
	[Riwayat_Alergi] [varchar](254) NULL,
	[Riwayat_Penyakit] [varchar](254) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_pembayaran]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_pembayaran](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_siswa] [int] NULL,
	[tanggalPembayaran] [date] NULL,
	[tipePembayaran] [varchar](50) NULL,
	[periodeSPP] [varchar](50) NULL,
	[deskripsi] [varchar](250) NULL,
	[status] [bit] NULL,
	[alasanPenolakan] [varchar](250) NULL,
	[buktiPembayaran] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ViewPembayaran]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewPembayaran] AS
SELECT
    B.Nama_Lengkap,
    A.deskripsi,
    A.tanggalPembayaran,
    A.periodeSPP,
    A.tipePembayaran,
    A.status,
    A.alasanPenolakan
FROM
    tbl_pembayaran AS A
    INNER JOIN tbl_siswa B ON A.id_siswa = B.id
GO
/****** Object:  Table [dbo].[tbl_dokumen_wali]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_dokumen_wali](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_ortu] [int] NULL,
	[kartukeluarga] [varchar](256) NULL,
	[ktp] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_dokumen_siswa]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_dokumen_siswa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_siswa] [int] NULL,
	[akta] [varchar](256) NULL,
	[foto] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_wali]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_wali](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_akun] [int] NULL,
	[nama_ayah] [varchar](100) NULL,
	[NIK_ayah] [varchar](16) NULL,
	[panggilan_ayah] [varchar](50) NULL,
	[pendidikan_ayah] [varchar](50) NULL,
	[pekerjaan_ayah] [varchar](50) NULL,
	[nama_ibu] [varchar](100) NULL,
	[NIK_ibu] [varchar](16) NULL,
	[panggilan_ibu] [varchar](50) NULL,
	[pendidikan_ibu] [varchar](50) NULL,
	[pekerjaan_ibu] [varchar](50) NULL,
	[No_KTP] [varchar](20) NULL,
	[No_HP] [varchar](15) NULL,
	[Alamat] [varchar](254) NULL,
	[tempatlahir_ayah] [varchar](50) NULL,
	[tanggallahir_ayah] [date] NULL,
	[tempatlahir_ibu] [varchar](50) NULL,
	[tanggallahir_ibu] [date] NULL,
 CONSTRAINT [PK__tbl_wali__3213E83FB3B4C5F4] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_akun]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_akun](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](254) NOT NULL,
	[hashPassword] [char](128) NOT NULL,
	[saltPassword] [char](128) NULL,
	[terverifikasi] [bit] NOT NULL,
	[tipeAkun] [varchar](10) NOT NULL,
 CONSTRAINT [PK__tbl_akun__3213E83F32262638] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Overview]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[Overview] AS 
select siswa.id as id_siswa, 
siswa.Nama_Lengkap, 
siswa.Nama_Panggilan, 
siswa.BirthPlace, 
siswa.BirtDate,
siswa.NIK,
siswa.jenis_kelamin,
siswa.Anak_Ke,
siswa.Jml_Saudara_Kandung,
siswa.Golongan_Darah,
siswa.Kelahiran,
siswa.ASI_Berapa_Lama,
siswa.Sufor_Berapa_Lama,
siswa.Riwayat_Alergi,
siswa.Riwayat_Penyakit,
wali.nama_ayah,
wali.panggilan_ayah,
wali.tempatlahir_ayah,
wali.tanggallahir_ayah,
wali.pendidikan_ayah,
wali.pekerjaan_ayah,
wali.NIK_ayah,
wali.nama_ibu,
wali.panggilan_ibu,
wali.tempatlahir_ibu,
wali.tanggallahir_ibu,
wali.pendidikan_ibu,
wali.pekerjaan_ibu,
wali.No_KTP,
wali.No_HP,
wali.Alamat,
siswa.id_kelas,
dokumen_siswa.foto,
dokumen_siswa.akta,
dokumen_wali.kartukeluarga,
dokumen_wali.ktp,
akun.email
from tbl_siswa siswa
Full join tbl_wali wali on siswa.id_ortu = wali.id
Full join tbl_akun akun on wali.id_akun = akun.id
Full join tbl_dokumen_siswa dokumen_siswa on dokumen_siswa.id_siswa = siswa.id 
full join tbl_dokumen_wali dokumen_wali on dokumen_wali.id_ortu = wali.id
GO
/****** Object:  Table [dbo].[tbl_log]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[acccountID] [int] NULL,
	[severity] [int] NULL,
	[message] [varchar](max) NULL,
	[time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_resetPasswordAkun]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_resetPasswordAkun](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAkun] [int] NULL,
	[key] [char](32) NOT NULL,
	[waktuKadaluarsa] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_verifikasiAkun]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_verifikasiAkun](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAkun] [int] NULL,
	[kodeVerifikasi] [char](32) NOT NULL,
	[waktuKadaluarsa] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_dokumen_siswa]  WITH CHECK ADD FOREIGN KEY([id_siswa])
REFERENCES [dbo].[tbl_siswa] ([id])
GO
ALTER TABLE [dbo].[tbl_dokumen_wali]  WITH CHECK ADD  CONSTRAINT [FK__tbl_dokum__id_or__300424B4] FOREIGN KEY([id_ortu])
REFERENCES [dbo].[tbl_wali] ([id])
GO
ALTER TABLE [dbo].[tbl_dokumen_wali] CHECK CONSTRAINT [FK__tbl_dokum__id_or__300424B4]
GO
ALTER TABLE [dbo].[tbl_pembayaran]  WITH CHECK ADD FOREIGN KEY([id_siswa])
REFERENCES [dbo].[tbl_siswa] ([id])
GO
ALTER TABLE [dbo].[tbl_resetPasswordAkun]  WITH CHECK ADD FOREIGN KEY([idAkun])
REFERENCES [dbo].[tbl_akun] ([id])
GO
ALTER TABLE [dbo].[tbl_siswa]  WITH CHECK ADD  CONSTRAINT [FK__tbl_siswa__id_or__2D27B809] FOREIGN KEY([id_ortu])
REFERENCES [dbo].[tbl_wali] ([id])
GO
ALTER TABLE [dbo].[tbl_siswa] CHECK CONSTRAINT [FK__tbl_siswa__id_or__2D27B809]
GO
ALTER TABLE [dbo].[tbl_verifikasiAkun]  WITH CHECK ADD FOREIGN KEY([idAkun])
REFERENCES [dbo].[tbl_akun] ([id])
GO
ALTER TABLE [dbo].[tbl_wali]  WITH CHECK ADD  CONSTRAINT [FK__tbl_wali__id_aku__2A4B4B5E] FOREIGN KEY([id_akun])
REFERENCES [dbo].[tbl_akun] ([id])
GO
ALTER TABLE [dbo].[tbl_wali] CHECK CONSTRAINT [FK__tbl_wali__id_aku__2A4B4B5E]
GO
/****** Object:  StoredProcedure [dbo].[deleteExpiredVerifyCode]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[deleteExpiredVerifyCode]
AS
BEGIN;

BEGIN transaction deleteVerifyTransaction;

BEGIN try;

DELETE FROM
    tbl_verifikasiAkun
WHERE
    waktuKadaluarsa < CURRENT_TIMESTAMP;

-- Select a row which already expires
DELETE tbl_akun
FROM
    tbl_akun
    LEFT JOIN tbl_verifikasiAkun ON tbl_akun.id = tbl_verifikasiAkun.idAkun
WHERE
    tbl_verifikasiAkun.id IS NULL
    AND tbl_akun.terverifikasi = 0;

COMMIT transaction deleteVerifyTransaction;

END try BEGIN catch;

ROLLBACK transaction deleteVerifyTransaction;

END catch;

END;
GO
/****** Object:  StoredProcedure [dbo].[deleteResetPasswordRequest]    Script Date: 11/2/2020 6:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[deleteResetPasswordRequest]
AS
BEGIN;
BEGIN transaction deleteResetPassword;

BEGIN try;

DELETE FROM
    tbl_resetPasswordAkun
WHERE
    waktuKadaluarsa < CURRENT_TIMESTAMP;

COMMIT transaction deleteResetPassword;

END try BEGIN catch;

ROLLBACK transaction deleteResetPassword;

END catch;
END;
GO
USE [master]
GO
ALTER DATABASE [db_panda_mandiri] SET  READ_WRITE 
GO
