USE [master]
GO
/****** Object:  Database [db_panda_mandiri]    Script Date: 10/30/2020 11:58:49 PM ******/
CREATE DATABASE [db_panda_mandiri]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_panda_mandiri', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_panda_mandiri.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'db_panda_mandiri_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_panda_mandiri_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [db_panda_mandiri] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_panda_mandiri].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_panda_mandiri] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_panda_mandiri] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_panda_mandiri] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_panda_mandiri] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_panda_mandiri] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET RECOVERY FULL 
GO
ALTER DATABASE [db_panda_mandiri] SET  MULTI_USER 
GO
ALTER DATABASE [db_panda_mandiri] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_panda_mandiri] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_panda_mandiri] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_panda_mandiri] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_panda_mandiri] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'db_panda_mandiri', N'ON'
GO
ALTER DATABASE [db_panda_mandiri] SET QUERY_STORE = OFF
GO
USE [db_panda_mandiri]
GO
/****** Object:  Table [dbo].[tbl_siswa]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_siswa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama_Lengkap] [varchar](100) NULL,
	[Nama_Panggilan] [varchar](20) NULL,
	[jenis_kelamin] [varchar](10) NULL,
	[id_ortu] [int] NULL,
	[id_kelas] [int] NULL,
	[BirthPlace] [varchar](25) NULL,
	[BirtDate] [datetime] NULL,
	[NIK] [varchar](20) NULL,
	[Anak_Ke] [int] NULL,
	[Jml_Saudara_Kandung] [int] NULL,
	[Golongan_Darah] [varchar](5) NULL,
	[Kelahiran] [varchar](10) NULL,
	[ASI_Berapa_Lama] [int] NULL,
	[Sufor_Berapa_Lama] [int] NULL,
	[Riwayat_Alergi] [varchar](254) NULL,
	[Riwayat_Penyakit] [varchar](254) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_pembayaran]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_pembayaran](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_siswa] [int] NULL,
	[tanggalPembayaran] [date] NULL,
	[tipePembayaran] [varchar](50) NULL,
	[periodeSPP] [varchar](50) NULL,
	[deskripsi] [varchar](250) NULL,
	[status] [bit] NULL,
	[alasanPenolakan] [varchar](250) NULL,
	[buktiPembayaran] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ViewPembayaran]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewPembayaran] AS
SELECT
    B.Nama_Lengkap,
    A.deskripsi,
    A.tanggalPembayaran,
    A.periodeSPP,
    A.tipePembayaran,
    A.status,
    A.alasanPenolakan
FROM
    tbl_pembayaran AS A
    INNER JOIN tbl_siswa B ON A.id_siswa = B.id
GO
/****** Object:  Table [dbo].[tbl_dokumen_wali]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_dokumen_wali](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_ortu] [int] NULL,
	[kartukeluarga] [varchar](256) NULL,
	[ktp] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_dokumen_siswa]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_dokumen_siswa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_siswa] [int] NULL,
	[akta] [varchar](256) NULL,
	[foto] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_wali]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_wali](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_akun] [int] NULL,
	[nama_ayah] [varchar](100) NULL,
	[NIK_ayah] [varchar](16) NULL,
	[panggilan_ayah] [varchar](50) NULL,
	[pendidikan_ayah] [varchar](50) NULL,
	[pekerjaan_ayah] [varchar](50) NULL,
	[nama_ibu] [varchar](100) NULL,
	[NIK_ibu] [varchar](16) NULL,
	[panggilan_ibu] [varchar](50) NULL,
	[pendidikan_ibu] [varchar](50) NULL,
	[pekerjaan_ibu] [varchar](50) NULL,
	[No_KTP] [varchar](20) NULL,
	[No_HP] [varchar](15) NULL,
	[Alamat] [varchar](254) NULL,
	[tempatlahir_ayah] [varchar](50) NULL,
	[tanggallahir_ayah] [date] NULL,
	[tempatlahir_ibu] [varchar](50) NULL,
	[tanggallahir_ibu] [date] NULL,
 CONSTRAINT [PK__tbl_wali__3213E83FB3B4C5F4] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_akun]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_akun](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](254) NOT NULL,
	[hashPassword] [char](128) NOT NULL,
	[saltPassword] [char](128) NULL,
	[terverifikasi] [bit] NOT NULL,
	[tipeAkun] [varchar](10) NOT NULL,
 CONSTRAINT [PK__tbl_akun__3213E83F32262638] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Overview]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[Overview] AS 
select siswa.id as id_siswa, 
siswa.Nama_Lengkap, 
siswa.Nama_Panggilan, 
siswa.BirthPlace, 
siswa.BirtDate,
siswa.NIK,
siswa.jenis_kelamin,
siswa.Anak_Ke,
siswa.Jml_Saudara_Kandung,
siswa.Golongan_Darah,
siswa.Kelahiran,
siswa.ASI_Berapa_Lama,
siswa.Sufor_Berapa_Lama,
siswa.Riwayat_Alergi,
siswa.Riwayat_Penyakit,
wali.nama_ayah,
wali.panggilan_ayah,
wali.tempatlahir_ayah,
wali.tanggallahir_ayah,
wali.pendidikan_ayah,
wali.pekerjaan_ayah,
wali.NIK_ayah,
wali.nama_ibu,
wali.panggilan_ibu,
wali.tempatlahir_ibu,
wali.tanggallahir_ibu,
wali.pendidikan_ibu,
wali.pekerjaan_ibu,
wali.No_KTP,
wali.No_HP,
wali.Alamat,
siswa.id_kelas,
dokumen_siswa.foto,
dokumen_siswa.akta,
dokumen_wali.kartukeluarga,
dokumen_wali.ktp,
akun.email
from tbl_siswa siswa
Full join tbl_wali wali on siswa.id_ortu = wali.id
Full join tbl_akun akun on wali.id_akun = akun.id
Full join tbl_dokumen_siswa dokumen_siswa on dokumen_siswa.id_siswa = siswa.id 
full join tbl_dokumen_wali dokumen_wali on dokumen_wali.id_ortu = wali.id
GO
/****** Object:  Table [dbo].[tbl_log]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[acccountID] [int] NULL,
	[severity] [int] NULL,
	[message] [varchar](MAX) NULL,
	[time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_resetPasswordAkun]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_resetPasswordAkun](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAkun] [int] NULL,
	[key] [char](32) NOT NULL,
	[waktuKadaluarsa] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_verifikasiAkun]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_verifikasiAkun](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idAkun] [int] NULL,
	[kodeVerifikasi] [char](32) NOT NULL,
	[waktuKadaluarsa] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbl_akun] ON 

INSERT [dbo].[tbl_akun] ([id], [email], [hashPassword], [saltPassword], [terverifikasi], [tipeAkun]) VALUES (2141, N'admin@pandamandiri.com', N'73710B3878DA571316519AC98CBB9164B8FCECAB0D6B1F7FF026E4C4447F5137D44FA4A2F12C03CBB99D5892223BFC802E3862204A54DA2C084ACF7C1C584F85', N'44C55D1A6FBF89476A93032F37EC051B24C8422321E51963622C904DAA8842554D44C7A060727C570B3282E037E61CAD0F6DE96EF742D5D05A40D37CA9F63782', 1, N'admin')
INSERT [dbo].[tbl_akun] ([id], [email], [hashPassword], [saltPassword], [terverifikasi], [tipeAkun]) VALUES (2142, N'parentexample@pandamandiri.com', N'103673CCD85FB0E56C47235B644EC59DB5C2FE4B24DA8569FF43207F02EC02A061BDC394ACD18EECCA471D58F257A06C2FF6488640A154DD2CCC5962A7FD65D0', N'1108395C4E27B25BD69E91EC3B8023D9AE5CA61AB4D17025DF8EB2386C5F6630D9732CE6A33C20EAEEDA69C11C5FB36F82DB1213D1DF8F19A35BBC400A7A5E01', 1, N'user')
SET IDENTITY_INSERT [dbo].[tbl_akun] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_dokumen_siswa] ON 

INSERT [dbo].[tbl_dokumen_siswa] ([id], [id_siswa], [akta], [foto]) VALUES (124, 129, N'C9BD995580740DC18EEBAFC5F6AD7461', N'C508DF5EDD78CC85C7690B6E2C70DF88')
SET IDENTITY_INSERT [dbo].[tbl_dokumen_siswa] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_dokumen_wali] ON 

INSERT [dbo].[tbl_dokumen_wali] ([id], [id_ortu], [kartukeluarga], [ktp]) VALUES (109, 111, N'0F8C2D0C98574FA0CEC14B0DFE651E80', N'8AAB3F6CC8BC320FA3ECB9802FCD3CFF')
SET IDENTITY_INSERT [dbo].[tbl_dokumen_wali] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_log] ON 

INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (96, 2141, 0, N'Account have login', CAST(N'2020-10-23T01:52:24.910' AS DateTime))
INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (97, 2141, 0, N'Account have logout', CAST(N'2020-10-23T01:52:28.007' AS DateTime))
INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (98, 2142, 0, N'Account have login', CAST(N'2020-10-23T01:53:58.180' AS DateTime))
INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (99, 2142, 2, N'A generic error occurred in GDI+.', CAST(N'2020-10-23T01:55:18.283' AS DateTime))
INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (100, 2142, 2, N'A generic error occurred in GDI+.', CAST(N'2020-10-23T01:55:26.973' AS DateTime))
INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (101, 2142, 0, N'Account tries to login with a wrong password', CAST(N'2020-10-23T02:09:54.493' AS DateTime))
INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (102, 2142, 0, N'Account have login', CAST(N'2020-10-23T02:09:58.610' AS DateTime))
INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (103, 2142, 0, N'Account have set their parent data', CAST(N'2020-10-23T02:10:41.573' AS DateTime))
INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (104, 2142, 0, N'Account have registered one of their child', CAST(N'2020-10-23T02:11:12.577' AS DateTime))
INSERT [dbo].[tbl_log] ([id], [acccountID], [severity], [message], [time]) VALUES (105, 2142, 0, N'Account have logout', CAST(N'2020-10-23T02:11:21.310' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_log] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_siswa] ON 

INSERT [dbo].[tbl_siswa] ([id], [Nama_Lengkap], [Nama_Panggilan], [jenis_kelamin], [id_ortu], [id_kelas], [BirthPlace], [BirtDate], [NIK], [Anak_Ke], [Jml_Saudara_Kandung], [Golongan_Darah], [Kelahiran], [ASI_Berapa_Lama], [Sufor_Berapa_Lama], [Riwayat_Alergi], [Riwayat_Penyakit]) VALUES (129, N'Whoever Else Idk                                                                                    ', N'Someone             ', N'Female    ', 111, 1, N'Jakarta                  ', CAST(N'2005-06-01T00:00:00.000' AS DateTime), N'6347573642631414    ', 2, 5, N'AB   ', N'Normal    ', 2, 15, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_siswa] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_wali] ON 

INSERT [dbo].[tbl_wali] ([id], [id_akun], [nama_ayah], [NIK_ayah], [panggilan_ayah], [pendidikan_ayah], [pekerjaan_ayah], [nama_ibu], [NIK_ibu], [panggilan_ibu], [pendidikan_ibu], [pekerjaan_ibu], [No_KTP], [No_HP], [Alamat], [tempatlahir_ayah], [tanggallahir_ayah], [tempatlahir_ibu], [tanggallahir_ibu]) VALUES (111, 2142, N'Imam Abdul Aziz', N'0215203523041242', N'Bro', N'STEM Graduate', N'Unemployed', N'Someone Else', N'0215203523041242', N'Idk who', N'STEM Graduate', N'CEO of Some Other Company', N'3213352323541241', N'089123412934', N'Idk, somewhere?', N'Jakarta', CAST(N'1980-06-10' AS Date), N'Nagasaki', CAST(N'1908-09-30' AS Date))
SET IDENTITY_INSERT [dbo].[tbl_wali] OFF
GO
ALTER TABLE [dbo].[tbl_dokumen_siswa]  WITH CHECK ADD FOREIGN KEY([id_siswa])
REFERENCES [dbo].[tbl_siswa] ([id])
GO
ALTER TABLE [dbo].[tbl_dokumen_wali]  WITH CHECK ADD  CONSTRAINT [FK__tbl_dokum__id_or__300424B4] FOREIGN KEY([id_ortu])
REFERENCES [dbo].[tbl_wali] ([id])
GO
ALTER TABLE [dbo].[tbl_dokumen_wali] CHECK CONSTRAINT [FK__tbl_dokum__id_or__300424B4]
GO
ALTER TABLE [dbo].[tbl_pembayaran]  WITH CHECK ADD FOREIGN KEY([id_siswa])
REFERENCES [dbo].[tbl_siswa] ([id])
GO
ALTER TABLE [dbo].[tbl_resetPasswordAkun]  WITH CHECK ADD FOREIGN KEY([idAkun])
REFERENCES [dbo].[tbl_akun] ([id])
GO
ALTER TABLE [dbo].[tbl_siswa]  WITH CHECK ADD  CONSTRAINT [FK__tbl_siswa__id_or__2D27B809] FOREIGN KEY([id_ortu])
REFERENCES [dbo].[tbl_wali] ([id])
GO
ALTER TABLE [dbo].[tbl_siswa] CHECK CONSTRAINT [FK__tbl_siswa__id_or__2D27B809]
GO
ALTER TABLE [dbo].[tbl_verifikasiAkun]  WITH CHECK ADD FOREIGN KEY([idAkun])
REFERENCES [dbo].[tbl_akun] ([id])
GO
ALTER TABLE [dbo].[tbl_wali]  WITH CHECK ADD  CONSTRAINT [FK__tbl_wali__id_aku__2A4B4B5E] FOREIGN KEY([id_akun])
REFERENCES [dbo].[tbl_akun] ([id])
GO
ALTER TABLE [dbo].[tbl_wali] CHECK CONSTRAINT [FK__tbl_wali__id_aku__2A4B4B5E]
GO
/****** Object:  StoredProcedure [dbo].[deleteExpiredVerifyCode]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[deleteExpiredVerifyCode] AS;

BEGIN;

BEGIN transaction deleteVerifyTransaction;

BEGIN try;

DELETE FROM
    tbl_verifikasiAkun
WHERE
    waktuKadaluarsa < CURRENT_TIMESTAMP;

-- Select a row which already expires
DELETE tbl_akun
FROM
    tbl_akun
    LEFT JOIN tbl_verifikasiAkun ON tbl_akun.id = tbl_verifikasiAkun.idAkun
WHERE
    tbl_verifikasiAkun.id IS NULL
    AND tbl_akun.terverifikasi = 0;

COMMIT transaction deleteVerifyTransaction;

END try BEGIN catch;

ROLLBACK transaction deleteVerifyTransaction;

END catch;

END;
GO
/****** Object:  StoredProcedure [dbo].[deleteResetPasswordRequest]    Script Date: 10/30/2020 11:58:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[deleteResetPasswordRequest]
AS
BEGIN;
BEGIN transaction deleteResetPassword;

BEGIN try;

DELETE FROM
    tbl_resetPasswordAkun
WHERE
    waktuKadaluarsa < CURRENT_TIMESTAMP;

COMMIT transaction deleteResetPassword;

END try BEGIN catch;

ROLLBACK transaction deleteResetPassword;

END catch;
END;
GO
USE [master]
GO
ALTER DATABASE [db_panda_mandiri] SET  READ_WRITE 
GO
