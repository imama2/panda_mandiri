Create view Overview AS 
select siswa.id as id_siswa, 
siswa.Nama_Lengkap, 
siswa.Nama_Panggilan, 
siswa.BirthPlace, 
siswa.BirtDate,
siswa.NIK,
siswa.jenis_kelamin,
siswa.Anak_Ke,
siswa.Jml_Saudara_Kandung,
siswa.Golongan_Darah,
siswa.Kelahiran,
siswa.ASI_Berapa_Lama,
siswa.Sufor_Berapa_Lama,
siswa.Riwayat_Alergi,
siswa.Riwayat_Penyakit,
wali.nama_ayah,
wali.panggilan_ayah,
wali.tempatlahir_ayah,
wali.tanggallahir_ayah,
wali.pendidikan_ayah,
wali.pekerjaan_ayah,
wali.NIK_ayah,
wali.nama_ibu,
wali.panggilan_ibu,
wali.tempatlahir_ibu,
wali.tanggallahir_ibu,
wali.pendidikan_ibu,
wali.pekerjaan_ibu,
wali.No_KTP,
wali.No_HP,
wali.Alamat,
siswa.id_kelas,
dokumen_siswa.foto,
dokumen_siswa.akta,
dokumen_wali.kartukeluarga,
dokumen_wali.ktp,
akun.email
from tbl_siswa siswa
Full join tbl_wali wali on siswa.id_ortu = wali.id
Full join tbl_akun akun on wali.id_akun = akun.id
Full join tbl_dokumen_siswa dokumen_siswa on dokumen_siswa.id_siswa = siswa.id 
full join tbl_dokumen_wali dokumen_wali on dokumen_wali.id_ortu = wali.id
-- cek tbl wali dan siswa--
select * from tbl_siswa

select * from tbl_wali

-- cek overview--
select * from Overview

