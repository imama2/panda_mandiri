﻿using System.Web;
using System.Web.Optimization;

namespace Panda_Mandiri
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery/plugin/debounce").Include(
                        "~/Scripts/jquery.ba-throttle-debounce.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/custom/js").Include(
                      "~/Scripts/script.min.js"));

            bundles.Add(new StyleBundle("~/bundles/custom/css").Include(
                      "~/Content/styles.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include(
                      "~/Scripts/bootstrap.js"));


            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/external/css/*.css", // Just copy all of them for now, we'll select some of them after final.
                      "~/Content/external/fonts/*.css"));
        }
    }
}
