﻿using System.IO;
using System.Web;

namespace Panda_Mandiri.Controllers
{
    public static class EmailHelper
    {
        public static string verificationEmailString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Views/Shared/VerificationTemplate.html"));
        public static string paymentEmailString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Views/Shared/paymentTemplate.html"));
        public static string resetPasswordEmailString = File.ReadAllText(HttpContext.Current.Server.MapPath("~/Views/Shared/resetPasswordTemplate.html"));

        public static string getVerificationEmail(string token)
        {
            return verificationEmailString
                .Replace("[REPLACELINK]", helper.getRoot() + "/home/verify?key=" + token);
        }

        public static string getPaymentEmail(string link, string action, string reason)
        {
            return paymentEmailString
                .Replace("[REPLACELINK]", link)
                .Replace("[ACTION]", action)
                .Replace("[REASONTEXT]", reason);
        }

        public static string getResetPasswordEmail(string key)
        {
            return resetPasswordEmailString
                .Replace("[REPLACELINK]", key);
        }
    }
}