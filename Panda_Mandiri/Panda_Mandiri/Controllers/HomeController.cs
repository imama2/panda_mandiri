﻿using Panda_Mandiri.Models;
using Panda_Mandiri.Models.Extended;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Panda_Mandiri.Controllers
{
    public class HomeController : Controller
    {
        private db_panda_mandiriEntities db = new db_panda_mandiriEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login(string redirect = "")
        {
            if (!string.IsNullOrWhiteSpace(redirect))
            {
                var controlllerName = redirect.Split('/')[1];

                if (Session["sesiAkun"] == null)
                {
                    // Add alert that user have to login or register first
                    List<Alert> alerts = new List<Alert>();
                    alerts.Add(new Alert($"You need to be logged in before accessing {controlllerName} page.", Severity.warning));

                    ViewBag.redirect = redirect;
                    ViewBag.alerts = alerts;
                    return View();
                }

                if ((Session["sesiAkun"] as modelAkun).tipeAkun.ToLower() != controlllerName)
                {
                    // Add alert that account didn't have an access to that page
                    List<Alert> alerts = new List<Alert>();
                    alerts.Add(new Alert($"You didn't have the permission to access that page.", Severity.danger));

                    ViewBag.alerts = alerts;
                    return View();
                }
            }

            ViewBag.redirect = redirect;
            return View();
        }

        public ActionResult AboutUs()
        {
            return View();
        }
        public ActionResult ClassActivity()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(modelAkun data, string redirect = "")
        {
            // Lakukan pengecekan kedalam database di bagian ini
            var dataUser = db.tbl_akun.Where(x => x.email == data.email.ToLower()).FirstOrDefault();

            // Cek user telah terverifikasi atau belum
            if (dataUser == null)
            {
                ViewBag.emailError = "Email not registered";
                return View();
            }

            // Cek user telah terverifikasi atau belum
            if (!dataUser.terverifikasi)
            {
                db.writeLog(dataUser.id, $"Unverified account tries to login", null, Severity.info);
                ViewBag.emailError = "Email hasn't verified";
                return View();
            }

            // Cek hash password sama atau tidak
            var salt = helper.hextobyte(dataUser.saltPassword); // Ambil salt dari database, ubah ke byte array
            var hash = helper.createHash(data.password, salt); // Lakukan proses hash dengan password yang didapat, menggunakan salt dari database
            var storedHash = helper.hextobyte(dataUser.hashPassword); // Ambil hasil proses hash dari database, diubah ke byte array

            Debug.WriteLine(helper.bytetohex(hash), helper.bytetohex(storedHash));

            if (!hash.SequenceEqual(storedHash)) // Cek jika hasil hash dengan hash yang disimpan di database sama atau tidak
            {
                db.writeLog(dataUser.id, $"Account tries to login with a wrong password", null, Severity.info);
                ViewBag.passwordError = "Incorrect password";
                return View();
            }

            // Kalau sudah sampai sini berarti semua pengecekan sudah lolos

            db.writeLog(dataUser.id, $"Account have login", null, Severity.info);

            modelAkun sesiAkun = data;
            sesiAkun.password = null;
            sesiAkun.confirmPassword = null;
            sesiAkun.tipeAkun = dataUser.tipeAkun;
            sesiAkun.id = dataUser.id;
            Session.Add("sesiAkun", sesiAkun);
            if (redirect != "")
            {
                return Redirect(redirect);
            }
            else
            {
                return RedirectToAction("Index", sesiAkun.tipeAkun);
            }
        }
        public ActionResult redirect()
        {
            var sesiAkun = (modelAkun)HttpContext.Session["sesiAkun"];
            return RedirectToAction("Index", sesiAkun.tipeAkun);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(modelAkun data)
        {
            // Lakukan pengecekan kedalam database di bagian ini
            var containsError = false;

            if (db.tbl_akun.Where(x => x.email == data.email.ToLower()).FirstOrDefault() != null)
            {
                ViewBag.emailError = "Email already exists!";
                containsError = true;
            }

            if (containsError)
            {
                return View();
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                tbl_akun newAccount = new tbl_akun();
                newAccount.email = data.email.ToLower();
                newAccount.tipeAkun = "user";
                newAccount.terverifikasi = false;

                // Buat salt untuk proses hashing
                var salt = helper.createSalt();

                // Proses password dengan hashing PBKDF2
                var hash = helper.createHash(data.password, salt);

                // Simpan salt dengan hash, yang diubah kedalam bentuk teks heksadesimal
                newAccount.saltPassword = helper.bytetohex(salt);
                newAccount.hashPassword = helper.bytetohex(hash);

                db.tbl_akun.Add(newAccount);

                // Buat kode verifikasi baru
                var token = helper.createToken(16); // 16 byte saat diubah ke teks jumlahnya jadi 32

                tbl_verifikasiAkun newVerification = new tbl_verifikasiAkun();
                newVerification.idAkun = newAccount.id; // Entity framework mengassign id sementara dari row yang baru dibuat. Ketika perubahan DB disimpan, id sementara tersebut akan diganti ke id permanen di tabel.
                newVerification.kodeVerifikasi = helper.bytetohex(token);
                //newVerification.waktuKadaluarsa = DateTime.Now.AddDays(1);
                newVerification.waktuKadaluarsa = DateTime.Now;

                db.tbl_verifikasiAkun.Add(newVerification);

                db.SaveChanges();

                db.writeLog(newAccount.id, $"Account has been registered", null, Severity.info);

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("mail@website.tk.com", "Dzakira Cahaya Islami");
                mail.To.Add(new MailAddress(data.email));

                mail.Subject = "Email Konfirmasi Website TK";
                mail.Body = EmailHelper.getVerificationEmail(helper.bytetohex(token));
                mail.IsBodyHtml = true;

                Task.Run(async () =>
                {
                    await helper.sendMessage(mail); // Log if failed to send message
                    mail.Dispose();
                });

                transaction.Commit();
            }

            return RedirectToActionPermanent("registerSuccess");
        }

        public ActionResult registerSuccess()
        {
            return View();
        }

        public ActionResult verify(string key)
        {
            var dataVerifikasi = db.tbl_verifikasiAkun.Where(x => x.kodeVerifikasi == key).FirstOrDefault();
            if (dataVerifikasi != null)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    var dataAkun = db.tbl_akun.Where(x => x.id == dataVerifikasi.idAkun).FirstOrDefault();

                    dataAkun.terverifikasi = true;
                    db.tbl_verifikasiAkun.Remove(dataVerifikasi);

                    db.SaveChanges();

                    ViewBag.isVerified = true;
                    ViewBag.verifiedEmail = dataAkun.email;

                    db.writeLog(dataAkun.id, $"Account has been verified", null, Severity.info);

                    transaction.Commit();
                }
            }

            return View();
        }

        public ActionResult HalamanConfirmEmail()
        {
            return View();
        }

        public ActionResult resetPassword(string key = "")
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                var resetPasswordRow = db.tbl_resetPasswordAkun.Where(x => x.key == key).FirstOrDefault();

                if (resetPasswordRow == null)
                {
                    return View("ResetPasswordResult");
                }

                ViewBag.isResetDialog = true;
                ViewBag.resetKey = key;
                return View("ResetPasswordForm");
            }

            return View();
        }

        [HttpPost]
        public ActionResult resetPassword(modelAkun data, string key, string newPassword)
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                return resetPasswordForm(key, newPassword);
            }

            var accountData = db.tbl_akun.Where(x => x.email == data.email).FirstOrDefault();

            var containsError = false;

            if (accountData == null)
            {
                ViewBag.emailError = "Email didn't exist in our database";
                containsError = true;
            }

            if (containsError)
            {
                return View();
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                var newResetKey = new tbl_resetPasswordAkun();
                newResetKey.idAkun = accountData.id; // Entity framework mengassign id sementara dari row yang baru dibuat. Ketika perubahan DB disimpan, id sementara tersebut akan diganti ke id permanen di tabel.
                newResetKey.key = helper.createTokenString(32);
                //newResetKey.waktuKadaluarsa = DateTime.Now.AddDays(1);
                newResetKey.waktuKadaluarsa = DateTime.Now;

                db.tbl_resetPasswordAkun.Add(newResetKey);

                db.SaveChanges();
                db.writeLog(accountData.id, $"Account has requested password reset", null, Severity.info);

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("mail@website.tk.com", "TK Apalah");
                mail.To.Add(new MailAddress(data.email));

                mail.Subject = "Account Reset Password Link";
                mail.Body = EmailHelper.getResetPasswordEmail(Url.Action("resetPassword", "home", new { key = newResetKey.key }, Request.Url.Scheme));
                mail.IsBodyHtml = true;

                Task.Run(async () =>
                {
                    await helper.sendMessage(mail); // Log if failed to send message
                    mail.Dispose();
                });

                transaction.Commit();
            }

            return View("ResetPasswordSuccess");
        }

        public ActionResult resetPasswordForm(string key, string newPassword)
        {
            var resetPasswordData = db.tbl_resetPasswordAkun.Where(x => x.key == key).FirstOrDefault();
            if (resetPasswordData != null)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    var dataAkun = db.tbl_akun.Where(x => x.id == resetPasswordData.idAkun).FirstOrDefault();

                    // Buat salt untuk proses hashing
                    var salt = helper.createSalt();

                    // Proses password dengan hashing PBKDF2
                    var hash = helper.createHash(newPassword, salt);

                    // Simpan salt dengan hash, yang diubah kedalam bentuk teks heksadesimal
                    dataAkun.saltPassword = helper.bytetohex(salt);
                    dataAkun.hashPassword = helper.bytetohex(hash);

                    db.tbl_resetPasswordAkun.Remove(resetPasswordData);

                    db.SaveChanges();

                    ViewBag.resetSuccess = true;

                    db.writeLog(dataAkun.id, $"Account password has been reset  ", null, Severity.info);

                    transaction.Commit();
                }
            }

            return View("resetPasswordResult");
        }
    }
}