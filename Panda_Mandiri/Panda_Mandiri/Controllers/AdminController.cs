﻿using Microsoft.Reporting.WebForms;
using Panda_Mandiri.Models;
using Panda_Mandiri.Models.Extended;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Panda_Mandiri.Controllers
{
    public class CheckAdminLoggedInFilter : ActionFilterAttribute
    {
        private db_panda_mandiriEntities db = new db_panda_mandiriEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var sesiAkun = (modelAkun)filterContext.HttpContext.Session["sesiAkun"];

            if (sesiAkun == null || sesiAkun.tipeAkun != "admin")
            {
                // Add query parameter for redirect function
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                queryString.Add("redirect", filterContext.HttpContext.Request.RawUrl);
                filterContext.Result = new RedirectResult("/Home/login?" + queryString.ToString());

                base.OnActionExecuting(filterContext);
                return;
            }

            base.OnActionExecuting(filterContext);
            return;
        }
    }

    [CheckAdminLoggedInFilter]
    public class AdminController : Controller
    {
        private db_panda_mandiriEntities db = new db_panda_mandiriEntities();
        // GET: Admin

        public ActionResult Index()
        {
            return View();
        }

        // GET: Admin/Details/5
        public ActionResult Details(int id)
        {
            return View(db.tbl_wali.Where(x => x.id == id).FirstOrDefault());
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult ViewDataSiswa()
        {
            return View(db.tbl_siswa.ToList());
        }

        public ActionResult ViewDataWali()
        {
            return View(db.tbl_wali.ToList());
        }

        public ActionResult Payments()
        {
            return View();
        }

        public ActionResult signout()
        {
            db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Account have logout", null, Severity.info);

            Session["sesiAkun"] = null;

            return RedirectToAction("index", "home");
        }

        public ActionResult success()
        {
            return View();
        }

        public ActionResult pendingPayments(int offset = 0, int rowAmount = 10)
        {
            try
            {
                // Basically limiting the rowAmount. Don't want those scraping sites to use our all our server resources.
                // Also make the parameter to non-negative number.
                rowAmount = rowAmount.limitBelow(100).limitAbove(1);
                offset = offset.limitAbove(0);

                var recordCount = db.tbl_pembayaran
                        .Where(x => x.status == null)
                        .Count();

                var paymentRecord = db.tbl_pembayaran
                    .OrderByDescending(x => x.tanggalPembayaran)
                    .Where(x => x.status == null)
                    .Skip(offset)
                    .Take(rowAmount);

                ViewBag.pendingPaymentList = paymentRecord;
                ViewBag.pendingPaymentListCount = recordCount;
                ViewBag.currentRowAmount = rowAmount;
                ViewBag.currentOffset = offset;

                TempData.AddOrReplace("refferer", "pendingPayments");

                //if (TempData.ContainsKey("refferer"))
                //    TempData["refferer"] = "pendingPayments";
                //else
                //    TempData.Add("refferer", "pendingPayments");

                if (
                    helper.isNull(paymentRecord) ||
                    helper.isNull(recordCount) ||
                    helper.isNull(rowAmount) ||
                    helper.isNull(offset)
                )
                {
                    throw new Exception("One of the value returns null on PendingPayments. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }
        public ActionResult ReportDataWali()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //ReportParameter[] reportParameter = new ReportParameter[1];
            //reportParmeter[0] = new ReportParmeter("Keyword", "Ken");
            report.ServerReport.ReportPath = "/ParentsReport";
            //report.ServerReport.SetParameters(reportParameter);
            report.SizeToReportContent = true;
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult ReportDataSiswa()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //ReportParameter[] reportParameter = new ReportParameter[1];
            //reportParmeter[0] = new ReportParmeter("Keyword", "Ken");
            report.ServerReport.ReportPath = "/StudentReport";
            //report.ServerReport.SetParameters(reportParameter);
            report.SizeToReportContent = true;
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult ReportDataPembayaran()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //ReportParameter[] reportParameter = new ReportParameter[1];
            //reportParmeter[0] = new ReportParmeter("Keyword", "Ken");
            report.ServerReport.ReportPath = "/PaymentsReport";
            //report.ServerReport.SetParameters(reportParameter);
            report.SizeToReportContent = true;
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult ReportErrorLog()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //ReportParameter[] reportParameter = new ReportParameter[1];
            //reportParmeter[0] = new ReportParmeter("Keyword", "Ken");
            report.ServerReport.ReportPath = "/ErrorLogReport";
            //report.ServerReport.SetParameters(reportParameter);
            report.SizeToReportContent = true;
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult completedPayments(int offset = 0, int rowAmount = 10)
        {
            try
            {
                // Basically limiting the rowAmount. Don't want those scraping sites to use our all our server resources.
                // Also make the parameter to non-negative number.
                rowAmount = rowAmount.limitBelow(100).limitAbove(1);
                offset = offset.limitAbove(0);

                var recordCount = db.tbl_pembayaran
                        .Where(x => x.status != null)
                        .Count();

                var paymentRecord = db.tbl_pembayaran
                    .OrderByDescending(x => x.tanggalPembayaran)
                    .Where(x => x.status != null)
                    .Skip(offset)
                    .Take(rowAmount);

                ViewBag.completedPaymentList = paymentRecord;
                ViewBag.completedPaymentListCount = recordCount;
                ViewBag.currentRowAmount = rowAmount;
                ViewBag.currentOffset = offset;

                TempData.AddOrReplace("refferer", "completedPayments");

                //if (TempData.ContainsKey("refferer"))
                //    TempData["refferer"] = "completedPayments";
                //else
                //    TempData.Add("refferer", "completedPayments");

                if (
                    helper.isNull(paymentRecord) ||
                    helper.isNull(recordCount) ||
                    helper.isNull(rowAmount) ||
                    helper.isNull(offset)
                )
                {
                    throw new Exception("One of the value returns null on CompletedPayments. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult paymentDetails(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                if (TempData["refferer"] == null)
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);

                return RedirectToAction(TempData["refferer"] as string, "Admin");
            }

            try
            {

                var paymentDetails = db.tbl_pembayaran.Where(x => x.id == integerID).FirstOrDefault();

                if (paymentDetails == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }

                var studentDetails = db.tbl_siswa.Where(x => x.id == paymentDetails.id_siswa).FirstOrDefault();
                var parentDetails = db.tbl_wali.Where(x => x.id == studentDetails.id_ortu).FirstOrDefault();

                ViewBag.paymentDetails = paymentDetails;
                ViewBag.studentDetails = studentDetails;
                ViewBag.parentDetails = parentDetails;
                ViewBag.refferer = TempData["refferer"];

                if (
                    helper.isNull(paymentDetails) ||
                    helper.isNull(studentDetails) ||
                    helper.isNull(parentDetails)
                )
                {
                    throw new Exception("One of the value returns null on PaymentDetails. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        public ActionResult paymentDetails(int id, string action, string reason)
        {
            var paymentDetails = db.tbl_pembayaran.Where(x => x.id == id).FirstOrDefault();

            if (paymentDetails == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    switch (action)
                    {
                        case "accept":
                            paymentDetails.status = true;
                            break;

                        case "reject":
                            paymentDetails.status = false;
                            paymentDetails.alasanPenolakan = reason;
                            break;

                        default:
                            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    db.SaveChanges();
                    transaction.Commit();

                    db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Account have {action}ed the transaction {id}" + (action == "reject" ? $" because of reason: {reason}." : ""), null,  Severity.info);

                    MailMessage mail = new MailMessage();
                    var studentData = db.tbl_siswa.Where(x => x.id == paymentDetails.id_siswa).FirstOrDefault();
                    var parentData = db.tbl_wali.Where(x => x.id == studentData.id_ortu).FirstOrDefault();
                    var accountData = db.tbl_akun.Where(x => x.id == parentData.id_akun).FirstOrDefault();

                    mail.From = new MailAddress("mail@website.tk.com", "TK Apalah");
                    mail.To.Add(new MailAddress(accountData.email));

                    mail.Subject = $"Payment #{id} {action}ed";
                    mail.Body = EmailHelper.getPaymentEmail(Url.Action("studentPaymentDetails", "User", new { @id = id }, Request.Url.Scheme), action, action == "reject" ? $" because of reason: {reason}" : "");
                    mail.IsBodyHtml = true;

                    Task.Run(async () =>
                    {
                        await helper.sendMessage(mail); // Log if failed to send message
                        mail.Dispose();
                    });

                    TempData.AddOrReplace("successMessage", $"Successfully {action}ed the transaction.");

                    return RedirectToAction("success", "Admin");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                }
            }
        }

        public ActionResult parentList(int offset = 0, int rowAmount = 10)
        {
            try
            {
                // Basically limiting the rowAmount. Don't want those scraping sites to use our all our server resources.
                // Also make the parameter to non-negative number.
                rowAmount = rowAmount.limitBelow(100).limitAbove(1);
                offset = offset.limitAbove(0);

                var recordCount = db.tbl_wali
                            .Count();

                var parentRecords = db.tbl_wali
                    .OrderByDescending(x => x.id)
                    .Skip(offset)
                    .Take(rowAmount);

                ViewBag.parentList = parentRecords;
                ViewBag.parentListCount = recordCount;
                ViewBag.currentRowAmount = rowAmount;
                ViewBag.currentOffset = offset;

                if (
                    helper.isNull(parentRecords) ||
                    helper.isNull(recordCount) ||
                    helper.isNull(rowAmount) ||
                    helper.isNull(offset)
                )
                {
                    throw new Exception("One of the value returns null on parentList. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult parentDetails(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            try
            {
                var parentDetails = db.tbl_wali.Where(x => x.id == integerID).FirstOrDefault();
                var childsData = db.tbl_siswa.Where(x => x.id_ortu == parentDetails.id);

                if (parentDetails == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }

                var parentDocumentDetails = db.tbl_dokumen_wali.Where(x => x.id_ortu == parentDetails.id).FirstOrDefault();
                ViewBag.parentDetails = parentDetails;
                ViewBag.parentDocumentDetails = parentDocumentDetails;
                ViewBag.childList = childsData;

                if (
                    helper.isNull(parentDetails) ||
                    helper.isNull(parentDocumentDetails)
                )
                {
                    throw new Exception("One of the value returns null on parentDetails. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult studentList(int offset = 0, int rowAmount = 10)
        {
            try
            {
                // Basically limiting the rowAmount. Don't want those scraping sites to use our all our server resources.
                // Also make the parameter to non-negative number.
                rowAmount = rowAmount.limitBelow(100).limitAbove(1);
                offset = offset.limitAbove(0);

                var recordCount = db.tbl_siswa
                            .Count();

                var studentRecords = db.tbl_siswa
                    .OrderByDescending(x => x.id)
                    .Skip(offset)
                    .Take(rowAmount);

                ViewBag.studentList = studentRecords;
                ViewBag.studentListCount = recordCount;
                ViewBag.currentRowAmount = rowAmount;
                ViewBag.currentOffset = offset;

                if (
                    helper.isNull(studentRecords) ||
                    helper.isNull(recordCount) ||
                    helper.isNull(rowAmount) ||
                    helper.isNull(offset)
                )
                {
                    throw new Exception("One of the value returns null on studentList. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult studentDetails(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            try
            {
                var studentDetails = db.tbl_siswa.Where(x => x.id == integerID).FirstOrDefault();

                if (studentDetails == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }

                var studentDocumentDetails = db.tbl_dokumen_siswa.Where(x => x.id_siswa == studentDetails.id).FirstOrDefault();
                ViewBag.studentDetails = studentDetails;
                ViewBag.studentDocumentDetails = studentDocumentDetails;

                if (
                    helper.isNull(studentDetails) ||
                    helper.isNull(studentDocumentDetails)
                )
                {
                    throw new Exception("One of the value returns null on studentDetails. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult log(
            string _severityFilter = "012",
            string _accountID = null,
            string _timeAfter = null,
            string _timeBefore = null,
            string _isDownload = "false",
            int rowAmount = 10,
            int offset = 0
        )
        {
            List<int> severityFilter;
            int? accountID = null;
            DateTime? timeAfter = null;
            DateTime? timeBefore = null;

            try
            {
                // Basically limiting the rowAmount. Don't want those scraping sites to use our all our server resources.
                // Also make the parameter to non-negative number.
                rowAmount = rowAmount.limitBelow(100).limitAbove(1);
                offset = offset.limitAbove(0);

                severityFilter = _severityFilter.Select(x => (int)char.GetNumericValue(x)).ToList();

                if (!string.IsNullOrWhiteSpace(_accountID))
                    accountID = int.Parse(_accountID);

                if (!string.IsNullOrWhiteSpace(_timeAfter))
                    timeAfter = DateTime.ParseExact(_timeAfter, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

                if (!string.IsNullOrWhiteSpace(_timeBefore))
                    timeBefore = DateTime.ParseExact(_timeBefore, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            var conditions = new List<Expression<Func<LogView, bool>>>();

            conditions.Add(x => severityFilter.Contains(x.severity ?? -1));

            if (accountID != null)
                conditions.Add(x => x.acccountID == accountID);

            if (timeAfter != null)
                conditions.Add(x => DbFunctions.TruncateTime(x.time) >= timeAfter);

            if (timeBefore != null)
                conditions.Add(x => DbFunctions.TruncateTime(x.time) <= timeBefore);

            IQueryable<LogView> logData = db.LogViews;

            foreach (var condition in conditions)
            {
                logData = logData.Where(condition);
            }

            if (_isDownload == "true")
            {
                logData = logData
                    .OrderByDescending(x => x.id)
                    .Take(100);

                string fileString = "";
                fileString += "ID DateTime AccountID AccountEmail Severity Message DetailedMessage";
                fileString += Environment.NewLine;

                foreach (var log in logData)
                {
                    fileString += $"{log.id} {log.time?.ToString("dd/MM/yyyy hh/mm/ss")} {log.acccountID} {log.email} {log.severityLevel} {log.message} {log.detailedMessage}";
                    fileString += Environment.NewLine;
                }

                return File(Encoding.UTF8.GetBytes(fileString), "application/octet-stream", $"LogWebsiteTK_{DateTime.Now.ToString("dd/MM/yyyy_HH:mm:ss")}.txt");
            }
            else
            {

                var recordCount = logData.Count();

                var logRecord = logData
                    .OrderByDescending(x => x.id)
                    .Skip(offset)
                    .Take(rowAmount);

                ViewBag.logData = logRecord;
                ViewBag.recordCount = recordCount;
                ViewBag.currentRowAmount = rowAmount;
                ViewBag.currentOffset = offset;
                return View();
            }
        }

        public ActionResult news() => View();

        [HttpPost, ValidateInput(false)]
        public ActionResult news(tbl_news data)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var newNews = new tbl_news();
                    newNews.subject = data.subject;
                    newNews.message = data.message;
                    newNews.timePublished = DateTime.Now;

                    db.tbl_news.Add(newNews);
                    db.SaveChanges();
                    transaction.Commit();

                    db.writeLog((Session["sesiAkun"] as modelAkun).id, "Account have created a new news", null, Severity.info);

                    TempData.AddOrReplace("successMessage", "Successfully submitted a news.");

                    return RedirectToAction("success", "admin");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                }
            }
        }

        public ActionResult newsList(int rowAmount = 10, int offset = 0)
        {
            try
            {
                // Basically limiting the rowAmount. Don't want those scraping sites to use our all our server resources.
                // Also make the parameter to non-negative number.
                rowAmount = rowAmount.limitBelow(100).limitAbove(1);
                offset = offset.limitAbove(0);

                var recordCount = db.tbl_news
                            .Count();

                var newsRecords = db.tbl_news
                    .OrderByDescending(x => x.Id)
                    .Skip(offset)
                    .Take(rowAmount);

                ViewBag.newsList = newsRecords;
                ViewBag.newsListCount = recordCount;
                ViewBag.currentRowAmount = rowAmount;
                ViewBag.currentOffset = offset;

                if (
                    helper.isNull(newsRecords) ||
                    helper.isNull(recordCount) ||
                    helper.isNull(rowAmount) ||
                    helper.isNull(offset)
                )
                {
                    throw new Exception("One of the value returns null on newsList. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult newsDetails(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            try
            {
                var newsDetails = db.tbl_news.Where(x => x.Id == integerID).FirstOrDefault();

                if (newsDetails == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }

                ViewBag.encodedContent = newsDetails.message;

                if (
                    helper.isNull(newsDetails)
                )
                {
                    throw new Exception("One of the value returns null on studentDetails. It shouldn't happen");
                }

                return View(newsDetails);
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        // POST: Admin/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}