﻿using Microsoft.Reporting.WebForms;
using Panda_Mandiri.Models;
using Panda_Mandiri.Models.Extended;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Panda_Mandiri.Controllers
{
    public class checkUserLoggedInFilter : ActionFilterAttribute
    {
        db_panda_mandiriEntities db = new db_panda_mandiriEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var sesiAkun = (modelAkun)filterContext.HttpContext.Session["sesiAkun"];
            if (sesiAkun == null || sesiAkun.tipeAkun != "user") // Redirect if not user is not in session, or not permitted to access.
            {
                // Add alert that user have to login or register first
                List<Alert> alerts = new List<Alert>();
                alerts.Add(new Alert("You need to be logged in before accesing user page.", Severity.warning));
                filterContext.RouteData.Values.Add("alerts", alerts);

                // Add query parameter for redirect function
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                queryString.Add("redirect", filterContext.HttpContext.Request.RawUrl.ToLower());
                filterContext.Result = new RedirectResult("/Home/login?" + queryString.ToString());

                base.OnActionExecuting(filterContext);
                return;
            }

            // Passthrough condition
            if (
                // If user is sending data to aturDataWali
                (
                    filterContext.HttpContext.Request.HttpMethod == "POST" &&
                    filterContext.HttpContext.Request.RawUrl.ToLower() == ("/user/aturdatawali")
                ) ||
                // If user is trying to signout
                filterContext.HttpContext.Request.RawUrl.ToLower() == "/user/signout"
            ) // 
            {
                base.OnActionExecuting(filterContext);
                return;
            }

            // Force redirect to aturDataWali
            if (db.tbl_wali.Where(x => x.id_akun == sesiAkun.id).FirstOrDefault() == null && filterContext.HttpContext.Request.RawUrl.ToLower() != "/user/aturdatawali")
            {
                filterContext.Result = new RedirectResult("/user/aturdatawali");

                base.OnActionExecuting(filterContext);
            }

            // Force redirect to homepage
            if (db.tbl_wali.Where(x => x.id_akun == sesiAkun.id).FirstOrDefault() != null && filterContext.HttpContext.Request.RawUrl.ToLower() == "/User/aturDataWali")
            {
                // Add alert that user can't edit wali data for a moment
                filterContext.Result = new RedirectResult("/User/index");

                base.OnActionExecuting(filterContext);
                return;
            }
        }
    }

    [checkUserLoggedInFilter]
    public class UserController : Controller
    {
        db_panda_mandiriEntities db = new db_panda_mandiriEntities();
        // GET: User
        public ActionResult Index()
        {
            var latestNews = db.tbl_news.OrderByDescending(x => x.timePublished).Take(10);

            ViewBag.latestNews = latestNews;

            return View();
        }

        public ActionResult signout()
        {
            db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Account have logout", null, Severity.info);

            Session["sesiAkun"] = null;

            return RedirectToAction("index", "home");
        }

        public ActionResult success()
        {
            return View();
        }

        public ActionResult Profiles()
        {
            return View();
        }

        public ActionResult aturDataWali()
        {
            return View();
        }

        [HttpPost]
        public ActionResult aturDataWali(modelWali data)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_wali newWali = new tbl_wali();

                    newWali.id_akun = (Session["sesiAkun"] as modelAkun).id;
                    newWali.nama_ayah = data.namaAyah;
                    newWali.NIK_ayah = data.NIKAyah;
                    newWali.panggilan_ayah = data.panggilanAyah;
                    newWali.pendidikan_ayah = data.pendidikanAyah;
                    newWali.pekerjaan_ayah = data.pekerjaanAyah;
                    newWali.nama_ibu = data.namaIbu;
                    newWali.NIK_ibu = data.NIKIbu;
                    newWali.panggilan_ibu = data.panggilanIbu;
                    newWali.pendidikan_ibu = data.pendidikanIbu;
                    newWali.pekerjaan_ibu = data.pekerjaanIbu;
                    newWali.No_KTP = data.noKTP;
                    newWali.No_HP = data.noHP;
                    newWali.Alamat = data.alamat;
                    newWali.tempatlahir_ayah = data.tempatLahirAyah;
                    newWali.tempatlahir_ibu = data.tempatLahirIbu;

                    DateTime convTanggalLahirAyah, convTanggalLahirIbu;
                    if (!DateTime.TryParseExact(data.tanggalLahirAyah, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out convTanggalLahirAyah))
                    {
                        // Date conversion error
                        transaction.Rollback();
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }
                    if (!DateTime.TryParseExact(data.tanggalLahirIbu, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out convTanggalLahirIbu))
                    {
                        // Date conversion error
                        transaction.Rollback();
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    newWali.tanggallahir_ayah = convTanggalLahirAyah;
                    newWali.tanggallahir_ibu = convTanggalLahirIbu;

                    db.tbl_wali.Add(newWali);

                    var newKartuKeluargaFilename = helper.createTokenString(32);
                    var newKTPFilename = helper.createTokenString(32);

                    var newKartuKeluargaFilepath = Path.Combine(Server.MapPath("~/SiteData/kartukeluarga/"), newKartuKeluargaFilename);
                    var newKTPFilepath = Path.Combine(Server.MapPath("~/SiteData/ktp/"), newKTPFilename);

                    using (Image imgKartuKeluarga = Image.FromStream(data.kartuKeluarga.InputStream))
                    {
                        try
                        {
                            imgKartuKeluarga.Save(newKartuKeluargaFilepath, ImageFormat.Jpeg);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                        }
                    }

                    using (Image imgKTP = Image.FromStream(data.ktp.InputStream))
                    {
                        try
                        {
                            imgKTP.Save(newKTPFilepath, ImageFormat.Jpeg);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                        }
                    }

                    tbl_dokumen_wali newDokumenWali = new tbl_dokumen_wali();
                    newDokumenWali.id_ortu = newWali.id;
                    newDokumenWali.ktp = newKTPFilename;
                    newDokumenWali.kartukeluarga = newKartuKeluargaFilename;

                    db.tbl_dokumen_wali.Add(newDokumenWali);

                    db.SaveChanges();
                    transaction.Commit();

                    db.writeLog((Session["sesiAkun"] as modelAkun).id, "Account have set their parent data", null, Severity.info);

                    TempData.AddOrReplace("successMessage", "Successfully saved the parents documents.");

                    return RedirectToAction("success", "user");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                }
            }
        }

        public ActionResult editDataWali()
        {
            var accountID = (Session["sesiAkun"] as modelAkun).id;
            var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();
            var parentDocumentData = db.tbl_dokumen_wali.Where(x => x.id_ortu == parentData.id).FirstOrDefault();
            // No need to check if parent data is exist. If it doesn't, user shouldn't be here anyway.

            ViewBag.parentData = parentData;
            ViewBag.parentDocumentData = parentDocumentData;

            return View();
        }

        [HttpPost]
        public ActionResult editDataWali(modelWali data)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var accountID = (Session["sesiAkun"] as modelAkun).id;
                    var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();

                    parentData.nama_ayah = data.namaAyah;
                    parentData.NIK_ayah = data.NIKAyah;
                    parentData.panggilan_ayah = data.panggilanAyah;
                    parentData.pendidikan_ayah = data.pendidikanAyah;
                    parentData.pekerjaan_ayah = data.pekerjaanAyah;
                    parentData.nama_ibu = data.namaIbu;
                    parentData.NIK_ibu = data.NIKIbu;
                    parentData.panggilan_ibu = data.panggilanIbu;
                    parentData.pendidikan_ibu = data.pendidikanIbu;
                    parentData.pekerjaan_ibu = data.pekerjaanIbu;
                    parentData.No_KTP = data.noKTP;
                    parentData.No_HP = data.noHP;
                    parentData.Alamat = data.alamat;
                    parentData.tempatlahir_ayah = data.tempatLahirAyah;
                    parentData.tempatlahir_ibu = data.tempatLahirIbu;

                    DateTime convTanggalLahirAyah, convTanggalLahirIbu;
                    if (!DateTime.TryParseExact(data.tanggalLahirAyah, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out convTanggalLahirAyah))
                    {
                        // Date conversion error
                        transaction.Rollback();
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }
                    if (!DateTime.TryParseExact(data.tanggalLahirIbu, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out convTanggalLahirIbu))
                    {
                        // Date conversion error
                        transaction.Rollback();
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    parentData.tanggallahir_ayah = convTanggalLahirAyah;
                    parentData.tanggallahir_ibu = convTanggalLahirIbu;

                    var parentDocumentData = db.tbl_dokumen_wali.Where(x => x.id_ortu == parentData.id).FirstOrDefault();

                    if (data.kartuKeluarga != null)
                    {
                        var existingKartuKeluargaFilepath = Path.Combine(Server.MapPath("~/SiteData/kartukeluarga/"), parentDocumentData.kartukeluarga);
                        using (Image imgKartuKeluarga = Image.FromStream(data.kartuKeluarga.InputStream))
                        {
                            try
                            {
                                imgKartuKeluarga.Save(existingKartuKeluargaFilepath, ImageFormat.Jpeg);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                            }
                        }
                    }

                    if (data.ktp != null)
                    {
                        var existingKTPFilepath = Path.Combine(Server.MapPath("~/SiteData/ktp/"), parentDocumentData.ktp);
                        using (Image imgKTP = Image.FromStream(data.ktp.InputStream))
                        {
                            try
                            {
                                imgKTP.Save(existingKTPFilepath, ImageFormat.Jpeg);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                            }
                        }
                    }

                    db.SaveChanges();
                    transaction.Commit();

                    db.writeLog((Session["sesiAkun"] as modelAkun).id, "Account have set their parent data", null, Severity.info);

                    TempData.AddOrReplace("successMessage", "Successfully saved the parents documents.");

                    return RedirectToAction("success", "user");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                }
            }
        }

        public ActionResult daftarkanSiswa()
        {
            return View();
        }

        [HttpPost]
        public ActionResult daftarkanSiswa(modelSiswa data)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var idAkun = (Session["sesiAkun"] as modelAkun).id;
                    tbl_siswa newSiswa = new tbl_siswa();

                    newSiswa.id_ortu = db.tbl_wali.Where(x => x.id_akun == idAkun).FirstOrDefault().id;
                    newSiswa.Nama_Lengkap = data.namaLengkap;
                    newSiswa.Nama_Panggilan = data.namaPanggilan;
                    newSiswa.jenis_kelamin = data.jenisKelamin;
                    newSiswa.BirthPlace = data.BirthPlace;
                    newSiswa.NIK = data.NIK;
                    newSiswa.Golongan_Darah = data.golDarah;
                    newSiswa.Kelahiran = data.tipeKelahiran;
                    newSiswa.Riwayat_Alergi = data.alergi;
                    newSiswa.Riwayat_Penyakit = data.penyakit;

                    int kelas, anakKe, jmlSaudara, berapaLamaASI, berapaLamaSuFor;

                    if (!int.TryParse(data.ClassID, out kelas))
                    {
                        transaction.Rollback();
                        db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Integer conversion error, can't convert {data.ClassID} to integer", null, Severity.danger);
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    if (!int.TryParse(data.anakKe, out anakKe))
                    {
                        transaction.Rollback();
                        db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Integer conversion error, can't convert {data.anakKe} to integer", null, Severity.danger);
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    if (!int.TryParse(data.jmlSaudara, out jmlSaudara))
                    {
                        transaction.Rollback();
                        db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Integer conversion error, can't convert {data.jmlSaudara} to integer", null, Severity.danger);
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    if (!int.TryParse(data.berapaLamaASI, out berapaLamaASI))
                    {
                        transaction.Rollback();
                        db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Integer conversion error, can't convert {data.berapaLamaASI} to integer", null, Severity.danger);
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    if (!int.TryParse(data.berapaLamaSuFor, out berapaLamaSuFor))
                    {
                        transaction.Rollback();
                        db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Integer conversion error, can't convert {data.berapaLamaSuFor} to integer", null, Severity.danger);
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    newSiswa.id_kelas = kelas;
                    newSiswa.Anak_Ke = anakKe;
                    newSiswa.Jml_Saudara_Kandung = jmlSaudara;
                    newSiswa.ASI_Berapa_Lama = berapaLamaASI;
                    newSiswa.Sufor_Berapa_Lama = berapaLamaSuFor;

                    DateTime birthDate;
                    if (!DateTime.TryParseExact(data.BirtDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
                    {
                        // Date conversion error
                        transaction.Rollback();
                        db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Date conversion error, can't convert {data.BirtDate} to date", null, Severity.danger);
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    newSiswa.BirtDate = birthDate;

                    db.tbl_siswa.Add(newSiswa);

                    var newAktaFilename = helper.createTokenString(32);
                    var newFotoFilename = helper.createTokenString(32);

                    var newAktaFilepath = Path.Combine(Server.MapPath("~/SiteData/akta/"), newAktaFilename);
                    var newFotoFilepath = Path.Combine(Server.MapPath("~/SiteData/foto/"), newFotoFilename);

                    using (Image imgAkta = Image.FromStream(data.akta.InputStream))
                    {
                        try
                        {
                            imgAkta.Save(newAktaFilepath, ImageFormat.Jpeg);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                        }
                    }

                    using (Image imgFoto = Image.FromStream(data.foto.InputStream))
                    {
                        try
                        {
                            imgFoto.Save(newFotoFilepath, ImageFormat.Jpeg);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                        }
                    }

                    tbl_dokumen_siswa newDokumenSiswa = new tbl_dokumen_siswa();
                    newDokumenSiswa.id_siswa = newSiswa.id;
                    newDokumenSiswa.akta = newAktaFilename;
                    newDokumenSiswa.foto = newFotoFilename;

                    db.tbl_dokumen_siswa.Add(newDokumenSiswa);

                    db.SaveChanges();
                    transaction.Commit();

                    db.writeLog((Session["sesiAkun"] as modelAkun).id, "Account have registered one of their child", null, Severity.info);

                    TempData.AddOrReplace("successMessage", "Successfully registered a student.");

                    return RedirectToAction("success", "user");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                }
            }
        }

        public ActionResult editSiswa(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            var accountID = (Session["sesiAkun"] as modelAkun).id;
            var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();
            var studentData = db.tbl_siswa
                .Where(x => x.id == integerID)
                .Where(x => parentData.id == x.id_ortu)
                .FirstOrDefault();

            if (studentData == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }

            var studentDocumentData = db.tbl_dokumen_siswa
                .Where(x => x.id_siswa == studentData.id)
                .FirstOrDefault();

            ViewBag.studentData = studentData;
            ViewBag.studentDocumentData = studentDocumentData;

            return View();
        }

        // Warning, this code below is dumb as f. Proceed at your own risk. 
        // Especially for the one who made this code, which is myself.
        // 
        // This is what you get when you're trying to sprint all the task 
        // in 3 days while chugging tons of energy drink in 2 AM.
        [HttpPost]
        public ActionResult editSiswa(modelSiswa data)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var accountID = (Session["sesiAkun"] as modelAkun).id;
                    var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();
                    var existingSiswa = db.tbl_siswa
                        .Where(x => x.id == data.dataSiswa.id)
                        .Where(x => parentData.id == x.id_ortu)
                        .FirstOrDefault();

                    if (existingSiswa == null)
                    {
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                    }

                    existingSiswa.Nama_Lengkap = data.dataSiswa.Nama_Lengkap;
                    existingSiswa.Nama_Panggilan = data.dataSiswa.Nama_Panggilan;
                    existingSiswa.jenis_kelamin = data.dataSiswa.jenis_kelamin;
                    existingSiswa.BirthPlace = data.dataSiswa.BirthPlace;
                    existingSiswa.NIK = data.dataSiswa.NIK;
                    existingSiswa.Golongan_Darah = data.dataSiswa.Golongan_Darah;
                    existingSiswa.Kelahiran = data.dataSiswa.Kelahiran;
                    existingSiswa.Riwayat_Alergi = data.dataSiswa.Riwayat_Alergi;
                    existingSiswa.Riwayat_Penyakit = data.dataSiswa.Riwayat_Penyakit;
                    existingSiswa.id_kelas = data.dataSiswa.id_kelas;
                    existingSiswa.Anak_Ke = data.dataSiswa.Anak_Ke;
                    existingSiswa.Jml_Saudara_Kandung = data.dataSiswa.Jml_Saudara_Kandung;
                    existingSiswa.ASI_Berapa_Lama = data.dataSiswa.ASI_Berapa_Lama;
                    existingSiswa.Sufor_Berapa_Lama = data.dataSiswa.Sufor_Berapa_Lama;

                    DateTime birthDate;
                    if (!DateTime.TryParseExact(data.BirtDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
                    {
                        // Date conversion error
                        transaction.Rollback();
                        db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Date conversion error, can't convert {data.BirtDate} to date", null, Severity.danger);
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                    }

                    existingSiswa.BirtDate = birthDate;

                    var existingDokumenSiswa = db.tbl_dokumen_siswa.Where(x => x.id_siswa == data.dataSiswa.id).FirstOrDefault();

                    if (!helper.isNull(data.akta))
                    {
                        var existingAktaFilepath = Path.Combine(Server.MapPath("~/SiteData/akta/"), existingDokumenSiswa.akta);
                        using (Image imgAkta = Image.FromStream(data.akta.InputStream))
                        {
                            try
                            {
                                imgAkta.Save(existingAktaFilepath, ImageFormat.Jpeg);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                            }
                        }
                    }

                    if (!helper.isNull(data.foto))
                    {
                        var existingFotoFilepath = Path.Combine(Server.MapPath("~/SiteData/foto/"), existingDokumenSiswa.foto);
                        using (Image imgFoto = Image.FromStream(data.foto.InputStream))
                        {
                            try
                            {
                                imgFoto.Save(existingFotoFilepath, ImageFormat.Jpeg);
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                            }
                        }
                    }

                    db.SaveChanges();
                    transaction.Commit();

                    db.writeLog((Session["sesiAkun"] as modelAkun).id, "Account have edited one of their child", null, Severity.info);

                    TempData.AddOrReplace("successMessage", "Successfully edited a student.");

                    return RedirectToAction("success", "user");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                }
            }
        }

        [HttpDelete]
        public ActionResult deleteSiswa(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            var accountID = (Session["sesiAkun"] as modelAkun).id;
            var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();
            var studentData = db.tbl_siswa
                .Where(x => x.id == integerID)
                .Where(x => parentData.id == x.id_ortu)
                .FirstOrDefault();

            if (studentData == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var studentDocument = db.tbl_dokumen_siswa.Where(x => x.id_siswa == studentData.id).FirstOrDefault();
                    var aktaFilepath = Path.Combine(Server.MapPath("~/SiteData/akta/"), studentDocument.akta);
                    var fotoFilepath = Path.Combine(Server.MapPath("~/SiteData/foto/"), studentDocument.foto);

                    if (System.IO.File.Exists(aktaFilepath))
                        System.IO.File.Delete(Path.Combine(Server.MapPath("~/SiteData/akta/"), studentDocument.akta));

                    if (System.IO.File.Exists(fotoFilepath))
                        System.IO.File.Delete(Path.Combine(Server.MapPath("~/SiteData/foto/"), studentDocument.foto));

                    db.tbl_dokumen_siswa.Remove(studentDocument);

                    db.tbl_pembayaran.RemoveRange(
                        db.tbl_pembayaran.Where(x => x.id_siswa == studentData.id)
                    );

                    db.tbl_siswa.Remove(studentData);

                    db.SaveChanges();
                    transaction.Commit();

                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                }
            }
        }

        public ActionResult ListCetakForm()
        {
            var idAkun = (Session["sesiAkun"] as modelAkun).id;
            var idOrtu = db.tbl_wali.Where(x => x.id_akun == idAkun).FirstOrDefault().id;
            var listSiswa = db.tbl_siswa.Where(x => x.id_ortu == idOrtu);

            ViewBag.daftarSiswa = listSiswa;

            if (
                helper.isNull(listSiswa)
            )
            {
                throw new Exception("One of the value returns null on studentListForPayment. It shouldn't happen");
            }

            return View();
        }
        public ActionResult CetakForm(string id, string isPrint = "false", string fallback = "false")
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            var accountID = (Session["sesiAkun"] as modelAkun).id;
            var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();
            var studentData = db.tbl_siswa
                .Where(x => x.id == integerID)
                .Where(x => parentData.id == x.id_ortu)
                .FirstOrDefault();
            if (studentData == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }

            ViewBag.studentData = studentData;
            ViewBag.parentData = parentData;

            if (isPrint == "true")
            {
                // Return print version
                return View("registrationForm");
            }
            else
            {
                if (fallback == "true")
                {
                    // Use the ReportViewer
                    string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
                    ReportViewer report = new ReportViewer();
                    report.ProcessingMode = ProcessingMode.Remote;
                    report.Width = report.Width = System.Web.UI.WebControls.Unit.Pixel(1100);
                    report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
                    ReportParameter[] reportParameter = new ReportParameter[1];
                    reportParameter[0] = new ReportParameter("id_siswa", Convert.ToString(integerID));
                    report.ServerReport.ReportPath = "/RegistrationFormReport";
                    report.ServerReport.SetParameters(reportParameter);
                    report.ServerReport.Refresh();
                    ViewBag.ReportViewer = report;
                }

                return View();
            }
        }
        public ActionResult studentListForPayment()
        {
            var idAkun = (Session["sesiAkun"] as modelAkun).id;
            var idOrtu = db.tbl_wali.Where(x => x.id_akun == idAkun).FirstOrDefault().id;
            var listSiswa = db.tbl_siswa.Where(x => x.id_ortu == idOrtu);

            ViewBag.daftarSiswa = listSiswa;

            if (
                helper.isNull(listSiswa)
            )
            {
                throw new Exception("One of the value returns null on studentListForPayment. It shouldn't happen");
            }

            return View();
        }

        public ActionResult pembayaranSiswa(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            var accountID = (Session["sesiAkun"] as modelAkun).id;
            var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();
            var recordSiswa = db.tbl_siswa.Where(x => x.id == integerID).Where(x => parentData.id == x.id_ortu).FirstOrDefault();

            if (recordSiswa == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }

            ViewBag.recordSiswa = recordSiswa;

            return View();
        }

        [HttpPost]
        public ActionResult pembayaranSiswa(int id, modelPembayaran data)
        {
            var accountID = (Session["sesiAkun"] as modelAkun).id;
            var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();
            var recordSiswa = db.tbl_siswa.Where(x => x.id == id).Where(x => parentData.id == x.id_ortu).FirstOrDefault();

            if (recordSiswa == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_pembayaran newPayment = new tbl_pembayaran();
                    newPayment.id_siswa = id;
                    newPayment.tanggalPembayaran = DateTime.Now;
                    newPayment.tipePembayaran = data.tipePembayaran;
                    newPayment.deskripsi = data.deskripsi;

                    if (data.tipePembayaran == "Tuition")
                    {
                        newPayment.periodeSPP = data.periodeSPP;
                    }

                    var newPaymentProofName = helper.createTokenString(32);

                    var newPaymentProofFilepath = Path.Combine(Server.MapPath("~/SiteData/buktipembayaran/"), newPaymentProofName);

                    using (Image imgPaymentProof = Image.FromStream(data.buktiPembayaran.InputStream))
                    {
                        try
                        {
                            imgPaymentProof.Save(newPaymentProofFilepath, ImageFormat.Jpeg);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                        }
                    }

                    newPayment.buktiPembayaran = newPaymentProofName;

                    db.tbl_pembayaran.Add(newPayment);
                    db.SaveChanges();
                    transaction.Commit();

                    db.writeLog((Session["sesiAkun"] as modelAkun).id, $"Account have added a transcation to student {id}", null, Severity.info);

                    TempData.AddOrReplace("successMessage", "Successfully submit a payment transaction.");

                    return RedirectToAction("success", "user");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
                }
            }

            return View();
        }

        public ActionResult studentListForPaymentList()
        {
            var idAkun = (Session["sesiAkun"] as modelAkun).id;
            var idOrtu = db.tbl_wali.Where(x => x.id_akun == idAkun).FirstOrDefault().id;
            var listSiswa = db.tbl_siswa.Where(x => x.id_ortu == idOrtu);

            ViewBag.daftarSiswa = listSiswa;
            return View();
        }

        public ActionResult studentPaymentList(string id, int offset = 0, int rowAmount = 10)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            var accountID = (Session["sesiAkun"] as modelAkun).id;
            var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();
            var recordSiswa = db.tbl_siswa.Where(x => x.id == integerID).Where(x => parentData.id == x.id_ortu).FirstOrDefault();

            if (recordSiswa == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }

            try
            {
                // Basically limiting the rowAmount. Don't want those scraping sites to use our all our server resources.
                // Also make the parameter to non-negative number.
                rowAmount = rowAmount.limitBelow(100).limitAbove(1);
                offset = offset.limitAbove(0);

                var recordCount = db.tbl_pembayaran
                            .Where(x => x.id_siswa == integerID)
                            .Count();

                var paymentRecord = db.tbl_pembayaran
                    .OrderByDescending(x => x.tanggalPembayaran)
                    .OrderBy(x => x.status != null)
                    .Where(x => x.id_siswa == integerID)
                    .Skip(offset)
                    .Take(rowAmount);

                ViewBag.studentPaymentList = paymentRecord;
                ViewBag.studentPaymentListCount = recordCount;
                ViewBag.currentRowAmount = rowAmount;
                ViewBag.currentOffset = offset;

                if (
                    helper.isNull(paymentRecord) ||
                    helper.isNull(recordCount) ||
                    helper.isNull(rowAmount) ||
                    helper.isNull(offset)
                )
                {
                    throw new Exception("One of the value returns null on studentPaymentList. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult studentPaymentDetails(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            try
            {
                int idAkun, idOrtu, idSiswa;

                try
                {
                    idAkun = (Session["sesiAkun"] as modelAkun).id;
                    idOrtu = db.tbl_wali.Where(x => x.id_akun == idAkun).FirstOrDefault().id;
                    idSiswa = db.tbl_siswa.Where(x => x.id_ortu == idOrtu).FirstOrDefault().id;
                }
                catch (Exception ex)
                {
                    // If something happened here, we know if the paymentDetails are either invalid, or should not be accessible by other user.
                    // Especially if it's the NullPointerException. 
                    db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }

                var paymentDetails = db.tbl_pembayaran.Where(x => x.id == integerID).Where(x => x.id_siswa == idSiswa).FirstOrDefault();

                if (paymentDetails == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }

                ViewBag.paymentDetails = paymentDetails;

                ViewBag.studentID = idSiswa; // Needed for those hardcoded breadcrumb navigation

                if (
                    helper.isNull(paymentDetails)
                )
                {
                    throw new Exception("One of the value returns null on studentPaymentList. It shouldn't happen");
                }

                return View();
            }
            catch (System.Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult studentList(int offset = 0, int rowAmount = 10)
        {
            try
            {
                var accountID = (Session["sesiAkun"] as modelAkun).id;
                var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();

                // Basically limiting the rowAmount. Don't want those scraping sites to use our all our server resources.
                // Also make the parameter to non-negative number.
                rowAmount = rowAmount.limitBelow(100).limitAbove(1);
                offset = offset.limitAbove(0);

                var recordCount = db.tbl_siswa
                    .Where(x => x.id_ortu == parentData.id)
                    .Count();

                var studentRecords = db.tbl_siswa
                    .Where(x => x.id_ortu == parentData.id)
                    .OrderByDescending(x => x.id)
                    .Skip(offset)
                    .Take(rowAmount);

                ViewBag.studentList = studentRecords;
                ViewBag.studentListCount = recordCount;
                ViewBag.currentRowAmount = rowAmount;
                ViewBag.currentOffset = offset;

                if (
                    helper.isNull(studentRecords) ||
                    helper.isNull(recordCount) ||
                    helper.isNull(rowAmount) ||
                    helper.isNull(offset)
                )
                {
                    throw new Exception("One of the value returns null on studentList. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult studentDetails(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            try
            {
                var accountID = (Session["sesiAkun"] as modelAkun).id;
                var parentData = db.tbl_wali.Where(x => x.id_akun == accountID).FirstOrDefault();
                var studentDetails = db.tbl_siswa.Where(x => x.id == integerID).Where(x => parentData.id == x.id_ortu).FirstOrDefault();

                if (studentDetails == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }

                var studentDocumentDetails = db.tbl_dokumen_siswa.Where(x => x.id_siswa == studentDetails.id).FirstOrDefault();
                ViewBag.studentDetails = studentDetails;
                ViewBag.studentDocumentDetails = studentDocumentDetails;

                if (
                    helper.isNull(studentDetails) ||
                    helper.isNull(studentDocumentDetails)
                )
                {
                    throw new Exception("One of the value returns null on studentDetails. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult newsList(int rowAmount = 10, int offset = 0)
        {
            try
            {
                // Basically limiting the rowAmount. Don't want those scraping sites to use our all our server resources.
                // Also make the parameter to non-negative number.
                rowAmount = rowAmount.limitBelow(100).limitAbove(1);
                offset = offset.limitAbove(0);

                var recordCount = db.tbl_news
                            .Count();

                var newsRecords = db.tbl_news
                    .OrderByDescending(x => x.Id)
                    .Skip(offset)
                    .Take(rowAmount);

                ViewBag.newsList = newsRecords;
                ViewBag.newsListCount = recordCount;
                ViewBag.currentRowAmount = rowAmount;
                ViewBag.currentOffset = offset;

                if (
                    helper.isNull(newsRecords) ||
                    helper.isNull(recordCount) ||
                    helper.isNull(rowAmount) ||
                    helper.isNull(offset)
                )
                {
                    throw new Exception("One of the value returns null on newsList. It shouldn't happen");
                }

                return View();
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }

        public ActionResult newsDetails(string id)
        {
            int integerID = -1;
            if (!int.TryParse(id, out integerID) || integerID < 0)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            try
            {
                var newsDetails = db.tbl_news.Where(x => x.Id == integerID).FirstOrDefault();

                if (newsDetails == null)
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
                }

                ViewBag.encodedContent = newsDetails.message;

                if (
                    helper.isNull(newsDetails)
                )
                {
                    throw new Exception("One of the value returns null on studentDetails. It shouldn't happen");
                }

                return View(newsDetails);
            }
            catch (Exception ex)
            {
                db.writeLog((Session["sesiAkun"] as modelAkun).id, ex.Message, ex.ToString(), Severity.danger);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError);
            }
        }
    }
}
