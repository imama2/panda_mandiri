﻿using System.Web.Mvc;

namespace Panda_Mandiri.Controllers
{
    public class errorController : Controller
    {
        // GET: error
        public ActionResult Index()
        {
            return View();
        }

        // GET: error
        public ActionResult notfound()
        {
            Response.StatusCode = 404;
            return View("NotFound");
        }
    }
}