﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Panda_Mandiri.Controllers
{
    public class publicAPIController : ApiController
    {
        [Route("api/image/{directory}/{id}")]
        [HttpGet]
        public HttpResponseMessage image(string id, string directory) //Include the directory!
        {
            var filePath = Path.Combine(
                System.Web.Hosting.HostingEnvironment.MapPath("~/SiteData/"),
                directory,
                id
            );

            if (!File.Exists(filePath))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(File.ReadAllBytes(filePath));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

            return response;
        }
    }
}
