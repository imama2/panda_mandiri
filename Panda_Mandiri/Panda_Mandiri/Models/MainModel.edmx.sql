
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/06/2020 04:33:46
-- Generated from EDMX file: Z:\GitRepos\Panda Mandiri 3\Panda_Mandiri\Panda_Mandiri\Models\MainModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [db_panda_mandiri];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__tbl_dokum__id_or__300424B4]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_dokumen_wali] DROP CONSTRAINT [FK__tbl_dokum__id_or__300424B4];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_dokum__id_si__32E0915F]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_dokumen_siswa] DROP CONSTRAINT [FK__tbl_dokum__id_si__32E0915F];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_pemba__id_si__5CD6CB2B]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_pembayaran] DROP CONSTRAINT [FK__tbl_pemba__id_si__5CD6CB2B];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_reset__idAku__18EBB532]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_resetPasswordAkun] DROP CONSTRAINT [FK__tbl_reset__idAku__18EBB532];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_siswa__id_or__2D27B809]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_siswa] DROP CONSTRAINT [FK__tbl_siswa__id_or__2D27B809];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_verif__idAku__4BAC3F29]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_verifikasiAkun] DROP CONSTRAINT [FK__tbl_verif__idAku__4BAC3F29];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_wali__id_aku__2A4B4B5E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_wali] DROP CONSTRAINT [FK__tbl_wali__id_aku__2A4B4B5E];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[tbl_akun]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_akun];
GO
IF OBJECT_ID(N'[dbo].[tbl_dokumen_siswa]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_dokumen_siswa];
GO
IF OBJECT_ID(N'[dbo].[tbl_dokumen_wali]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_dokumen_wali];
GO
IF OBJECT_ID(N'[dbo].[tbl_log]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_log];
GO
IF OBJECT_ID(N'[dbo].[tbl_pembayaran]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_pembayaran];
GO
IF OBJECT_ID(N'[dbo].[tbl_resetPasswordAkun]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_resetPasswordAkun];
GO
IF OBJECT_ID(N'[dbo].[tbl_siswa]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_siswa];
GO
IF OBJECT_ID(N'[dbo].[tbl_verifikasiAkun]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_verifikasiAkun];
GO
IF OBJECT_ID(N'[dbo].[tbl_wali]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_wali];
GO
IF OBJECT_ID(N'[db_panda_mandiriModelStoreContainer].[LogView]', 'U') IS NOT NULL
    DROP TABLE [db_panda_mandiriModelStoreContainer].[LogView];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'tbl_akun'
CREATE TABLE [dbo].[tbl_akun] (
    [id] int IDENTITY(1,1) NOT NULL,
    [email] varchar(254)  NOT NULL,
    [hashPassword] char(128)  NOT NULL,
    [terverifikasi] bit  NOT NULL,
    [tipeAkun] varchar(10)  NOT NULL,
    [saltPassword] char(128)  NULL
);
GO

-- Creating table 'tbl_dokumen_siswa'
CREATE TABLE [dbo].[tbl_dokumen_siswa] (
    [id] int IDENTITY(1,1) NOT NULL,
    [id_siswa] int  NULL,
    [akta] varchar(256)  NULL,
    [foto] varchar(256)  NULL
);
GO

-- Creating table 'tbl_dokumen_wali'
CREATE TABLE [dbo].[tbl_dokumen_wali] (
    [id] int IDENTITY(1,1) NOT NULL,
    [id_ortu] int  NULL,
    [kartukeluarga] varchar(256)  NULL,
    [ktp] varchar(256)  NULL
);
GO

-- Creating table 'tbl_siswa'
CREATE TABLE [dbo].[tbl_siswa] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Nama_Lengkap] varchar(100)  NULL,
    [Nama_Panggilan] varchar(20)  NULL,
    [jenis_kelamin] varchar(10)  NULL,
    [id_ortu] int  NULL,
    [id_kelas] int  NULL,
    [BirthPlace] varchar(25)  NULL,
    [BirtDate] datetime  NULL,
    [NIK] varchar(20)  NULL,
    [Anak_Ke] int  NULL,
    [Jml_Saudara_Kandung] int  NULL,
    [Golongan_Darah] varchar(5)  NULL,
    [Kelahiran] varchar(20)  NULL,
    [ASI_Berapa_Lama] int  NULL,
    [Sufor_Berapa_Lama] int  NULL,
    [Riwayat_Alergi] varchar(254)  NULL,
    [Riwayat_Penyakit] varchar(254)  NULL
);
GO

-- Creating table 'tbl_wali'
CREATE TABLE [dbo].[tbl_wali] (
    [id] int IDENTITY(1,1) NOT NULL,
    [id_akun] int  NULL,
    [nama_ayah] varchar(100)  NULL,
    [NIK_ayah] varchar(16)  NULL,
    [panggilan_ayah] varchar(50)  NULL,
    [pendidikan_ayah] varchar(50)  NULL,
    [pekerjaan_ayah] varchar(50)  NULL,
    [nama_ibu] varchar(100)  NULL,
    [NIK_ibu] varchar(16)  NULL,
    [panggilan_ibu] varchar(50)  NULL,
    [pendidikan_ibu] varchar(50)  NULL,
    [pekerjaan_ibu] varchar(50)  NULL,
    [No_KTP] varchar(20)  NULL,
    [No_HP] varchar(15)  NULL,
    [Alamat] varchar(254)  NULL,
    [tempatlahir_ayah] varchar(50)  NULL,
    [tanggallahir_ayah] datetime  NULL,
    [tempatlahir_ibu] varchar(50)  NULL,
    [tanggallahir_ibu] datetime  NULL
);
GO

-- Creating table 'tbl_verifikasiAkun'
CREATE TABLE [dbo].[tbl_verifikasiAkun] (
    [id] int IDENTITY(1,1) NOT NULL,
    [idAkun] int  NULL,
    [kodeVerifikasi] char(32)  NOT NULL,
    [waktuKadaluarsa] datetime  NOT NULL
);
GO

-- Creating table 'tbl_pembayaran'
CREATE TABLE [dbo].[tbl_pembayaran] (
    [id] int IDENTITY(1,1) NOT NULL,
    [id_siswa] int  NULL,
    [tanggalPembayaran] datetime  NULL,
    [periodeSPP] varchar(50)  NULL,
    [deskripsi] varchar(250)  NULL,
    [status] bit  NULL,
    [alasanPenolakan] varchar(250)  NULL,
    [buktiPembayaran] varchar(250)  NULL,
    [tipePembayaran] varchar(50)  NULL
);
GO

-- Creating table 'tbl_resetPasswordAkun'
CREATE TABLE [dbo].[tbl_resetPasswordAkun] (
    [id] int IDENTITY(1,1) NOT NULL,
    [idAkun] int  NULL,
    [key] char(32)  NOT NULL,
    [waktuKadaluarsa] datetime  NOT NULL
);
GO

-- Creating table 'tbl_log'
CREATE TABLE [dbo].[tbl_log] (
    [id] int IDENTITY(1,1) NOT NULL,
    [acccountID] int  NULL,
    [severity] int  NULL,
    [message] varchar(500)  NULL,
    [time] datetime  NULL,
    [detailedMessage] varchar(max)  NULL
);
GO

-- Creating table 'LogViews'
CREATE TABLE [dbo].[LogViews] (
    [id] int  NOT NULL,
    [time] datetime  NULL,
    [email] varchar(254)  NOT NULL,
    [acccountID] int  NULL,
    [severity] int  NULL,
    [severityLevel] varchar(7)  NOT NULL,
    [message] varchar(500)  NULL,
    [detailedMessage] varchar(max)  NULL
);
GO

-- Creating table 'tbl_news'
CREATE TABLE [dbo].[tbl_news] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [timePublished] datetime  NOT NULL,
    [message] nvarchar(max)  NOT NULL,
    [subject] nvarchar(200)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'tbl_akun'
ALTER TABLE [dbo].[tbl_akun]
ADD CONSTRAINT [PK_tbl_akun]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_dokumen_siswa'
ALTER TABLE [dbo].[tbl_dokumen_siswa]
ADD CONSTRAINT [PK_tbl_dokumen_siswa]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_dokumen_wali'
ALTER TABLE [dbo].[tbl_dokumen_wali]
ADD CONSTRAINT [PK_tbl_dokumen_wali]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_siswa'
ALTER TABLE [dbo].[tbl_siswa]
ADD CONSTRAINT [PK_tbl_siswa]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_wali'
ALTER TABLE [dbo].[tbl_wali]
ADD CONSTRAINT [PK_tbl_wali]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_verifikasiAkun'
ALTER TABLE [dbo].[tbl_verifikasiAkun]
ADD CONSTRAINT [PK_tbl_verifikasiAkun]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_pembayaran'
ALTER TABLE [dbo].[tbl_pembayaran]
ADD CONSTRAINT [PK_tbl_pembayaran]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_resetPasswordAkun'
ALTER TABLE [dbo].[tbl_resetPasswordAkun]
ADD CONSTRAINT [PK_tbl_resetPasswordAkun]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_log'
ALTER TABLE [dbo].[tbl_log]
ADD CONSTRAINT [PK_tbl_log]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id], [email], [severityLevel] in table 'LogViews'
ALTER TABLE [dbo].[LogViews]
ADD CONSTRAINT [PK_LogViews]
    PRIMARY KEY CLUSTERED ([id], [email], [severityLevel] ASC);
GO

-- Creating primary key on [Id] in table 'tbl_news'
ALTER TABLE [dbo].[tbl_news]
ADD CONSTRAINT [PK_tbl_news]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [id_akun] in table 'tbl_wali'
ALTER TABLE [dbo].[tbl_wali]
ADD CONSTRAINT [FK__tbl_wali__id_aku__2A4B4B5E]
    FOREIGN KEY ([id_akun])
    REFERENCES [dbo].[tbl_akun]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_wali__id_aku__2A4B4B5E'
CREATE INDEX [IX_FK__tbl_wali__id_aku__2A4B4B5E]
ON [dbo].[tbl_wali]
    ([id_akun]);
GO

-- Creating foreign key on [id_siswa] in table 'tbl_dokumen_siswa'
ALTER TABLE [dbo].[tbl_dokumen_siswa]
ADD CONSTRAINT [FK__tbl_dokum__id_si__32E0915F]
    FOREIGN KEY ([id_siswa])
    REFERENCES [dbo].[tbl_siswa]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_dokum__id_si__32E0915F'
CREATE INDEX [IX_FK__tbl_dokum__id_si__32E0915F]
ON [dbo].[tbl_dokumen_siswa]
    ([id_siswa]);
GO

-- Creating foreign key on [id_ortu] in table 'tbl_dokumen_wali'
ALTER TABLE [dbo].[tbl_dokumen_wali]
ADD CONSTRAINT [FK__tbl_dokum__id_or__300424B4]
    FOREIGN KEY ([id_ortu])
    REFERENCES [dbo].[tbl_wali]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_dokum__id_or__300424B4'
CREATE INDEX [IX_FK__tbl_dokum__id_or__300424B4]
ON [dbo].[tbl_dokumen_wali]
    ([id_ortu]);
GO

-- Creating foreign key on [id_ortu] in table 'tbl_siswa'
ALTER TABLE [dbo].[tbl_siswa]
ADD CONSTRAINT [FK__tbl_siswa__id_or__2D27B809]
    FOREIGN KEY ([id_ortu])
    REFERENCES [dbo].[tbl_wali]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_siswa__id_or__2D27B809'
CREATE INDEX [IX_FK__tbl_siswa__id_or__2D27B809]
ON [dbo].[tbl_siswa]
    ([id_ortu]);
GO

-- Creating foreign key on [idAkun] in table 'tbl_verifikasiAkun'
ALTER TABLE [dbo].[tbl_verifikasiAkun]
ADD CONSTRAINT [FK__tbl_verif__idAku__4BAC3F29]
    FOREIGN KEY ([idAkun])
    REFERENCES [dbo].[tbl_akun]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_verif__idAku__4BAC3F29'
CREATE INDEX [IX_FK__tbl_verif__idAku__4BAC3F29]
ON [dbo].[tbl_verifikasiAkun]
    ([idAkun]);
GO

-- Creating foreign key on [id_siswa] in table 'tbl_pembayaran'
ALTER TABLE [dbo].[tbl_pembayaran]
ADD CONSTRAINT [FK__tbl_pemba__id_si__5CD6CB2B]
    FOREIGN KEY ([id_siswa])
    REFERENCES [dbo].[tbl_siswa]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_pemba__id_si__5CD6CB2B'
CREATE INDEX [IX_FK__tbl_pemba__id_si__5CD6CB2B]
ON [dbo].[tbl_pembayaran]
    ([id_siswa]);
GO

-- Creating foreign key on [idAkun] in table 'tbl_resetPasswordAkun'
ALTER TABLE [dbo].[tbl_resetPasswordAkun]
ADD CONSTRAINT [FK__tbl_reset__idAku__18EBB532]
    FOREIGN KEY ([idAkun])
    REFERENCES [dbo].[tbl_akun]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_reset__idAku__18EBB532'
CREATE INDEX [IX_FK__tbl_reset__idAku__18EBB532]
ON [dbo].[tbl_resetPasswordAkun]
    ([idAkun]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------