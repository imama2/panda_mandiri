﻿namespace Panda_Mandiri.Models.Extended
{
    public class modelAkun
    {
        public int id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string confirmPassword { get; set; }
        public bool terverifikasi { get; set; }
        public string tipeAkun { get; set; }
        public System.DateTime waktuKadaluarsa { get; set; }
    }
}