﻿using System.ComponentModel;
using System.Web;

namespace Panda_Mandiri.Models.Extended
{
    public class modelPembayaran
    {
        [DisplayName("Payment Type")]
        public string tipePembayaran { get; set; }
        [DisplayName("SPP Periode")]
        public string periodeSPP { get; set; }
        [DisplayName("Description")]
        public string deskripsi { get; set; }

        [DisplayName("Payment Proof")]
        public HttpPostedFileBase buktiPembayaran { get; set; }
    }
}