﻿using System.ComponentModel;
using System.Web;

namespace Panda_Mandiri.Models.Extended
{
    public class modelWali
    {
        // Semua harus dalam bentuk string agar JQueryValidation tidak komplain saat verifikasi
        [DisplayName("Father's Name")]
        public string namaAyah { get; set; }
        [DisplayName("Father's Nickname")]
        public string panggilanAyah { get; set; }
        [DisplayName("Father's NIK")]
        public string NIKAyah { get; set; }
        [DisplayName("Father's Birthplace")]
        public string tempatLahirAyah { get; set; }
        [DisplayName("Father's Birthdate")]
        public string tanggalLahirAyah { get; set; }
        [DisplayName("Father's Qualification")]
        public string pendidikanAyah { get; set; }
        [DisplayName("Father's Occupation")]
        public string pekerjaanAyah { get; set; }

        [DisplayName("Mother's Name")]
        public string namaIbu { get; set; }
        [DisplayName("Mother's Nickname")]
        public string panggilanIbu { get; set; }
        [DisplayName("Mother's NIK")]
        public string NIKIbu { get; set; }
        [DisplayName("Mother's Birthplace")]
        public string tempatLahirIbu { get; set; }
        [DisplayName("Mother's Birthdate")]
        public string tanggalLahirIbu { get; set; }
        [DisplayName("Mother's Qualification")]
        public string pendidikanIbu { get; set; }
        [DisplayName("Mother's Occupation")]
        public string pekerjaanIbu { get; set; }

        [DisplayName("Identification Number (KTP)")]
        public string noKTP { get; set; }
        [DisplayName("Phone Number")]
        public string noHP { get; set; }
        [DisplayName("Address")]
        public string alamat { get; set; }

        [DisplayName("Family Card (Kartu Keluarga)")]
        public HttpPostedFileBase kartuKeluarga { get; set; }
        [DisplayName("Identification Card (KTP)")]
        public HttpPostedFileBase ktp { get; set; }
    }
}