﻿namespace Panda_Mandiri.Models.Extended
{
    public class modelSesiAkun
    {
        public int id { get; set; }
        public string email { get; set; }
        public string tipeAkun { get; set; }
    }
}