﻿using System.ComponentModel;
using System.Web;

namespace Panda_Mandiri.Models.Extended
{
    public class modelSiswa
    {
        [DisplayName("Full Name")]
        public string namaLengkap { get; set; }
        [DisplayName("Nickname")]
        public string namaPanggilan { get; set; }
        [DisplayName("Sex")]
        public string jenisKelamin { get; set; }
        [DisplayName("Birth Place")]
        public string BirthPlace { get; set; }
        [DisplayName("Birth Date")]
        public string BirtDate { get; set; }
        [DisplayName("Class")]
        public string ClassID { get; set; }
        [DisplayName("ID Number")]
        public string NIK { get; set; }
        [DisplayName("What order do you come in your family?")]
        public string anakKe { get; set; }
        [DisplayName("How many siblings in your family?")]
        public string jmlSaudara { get; set; }
        [DisplayName("Blood Type")]
        public string golDarah { get; set; }
        [DisplayName("Birth Type")]
        public string tipeKelahiran { get; set; }
        [DisplayName("How long Breastfeeding?")]
        public string berapaLamaASI { get; set; }
        [DisplayName("How long milk formula?")]
        public string berapaLamaSuFor { get; set; }
        [DisplayName("Allergy History")]
        public string alergi { get; set; }
        [DisplayName("Sickness History")]
        public string penyakit { get; set; }

        [DisplayName("Birt Certificate")]
        public HttpPostedFileBase akta { get; set; }
        [DisplayName("Personal Photo")]
        public HttpPostedFileBase foto { get; set; }

        public tbl_siswa dataSiswa { get; set; }
        public tbl_dokumen_siswa dataDokumenSiswa { get; set; }
    }
}
