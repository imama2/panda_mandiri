# Notes

## Names

- Database name: db_panda_mandiri
- Main Project: Panda_Mandiri
- Service Name: Panda Mandiri Service

## Directories

- Main Website: ./Panda_Mandiri/Panda_Mandiri
- Service: ./Panda_Mandiri/Panda_Mandiri_Service
- Reporting File: ./Panda_Mandiri/Reporting Service File
- SQL Queries: ./Panda_Mandiri/SQL Queries

## Website Credentials

- User Account
  username: parentexample@pandamandiri.com
  password: asdf1234*

- Admin Account
  username: admin@pandamandiri.com
  password: asdasd

## Publishing Notes

When you're publishing the project, you will encounter an error at startup. This website needs some additional directories to be exist and accessible by the website for it to function. This next steps assumes you to understand basic publishing knowledge to continue.
    1. Find your website Application Pools. This was set during the Add Website process the same as your app name, so little change you forget the name. But if you set your own application pool and you forgot which one, try this <https://onlinehelp.coveo.com/en/ces/7.0/administrator/finding_the_name_of_the_user_that_runs_a_process_in_iis.htm>
    2. Open your website by clicking your website name in the left pane, and then click Edit Permission on the action pane on the right side.
    3. Select the Security tab, click the Advanced button.
    4. Find the name of your application pool on the list, and then double-click it.
    5. Select all permission by checking Full Control checkbox, and the press OK.
    6. Press OK for the next window, and that's it! If you try to access the website, it will run fine.
