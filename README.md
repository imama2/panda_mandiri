# Panda Mandiri Website Repository

This repository contains all the stuff you need to deploy your own version of Panda Mandiri

## Installation

See [INSTALL.md](./INSTALL.md), or the text version: [INSTALL.txt](./INSTALL.txt)

## Notes

See [NOTES.md](./NOTES.md), or the text version: [NOTES.txt](./NOTES.txt)
